#define DEBUG
#include "nocrypt.c"

#define GENERATOR 2
#define PRIME_SIZE 64
#define PRIVATE_KEY_SIZE 32

#define HASH_SIZE 4
#define HASH_ITERS 100000

#define CRYPTO_KEY_STR "wat"
#define CRYPTO_KEY_SIZE 16
#define CRYPTO_PLAINTEXT "It's the Jews."

int dh_test(ssize_t prime_size, ssize_t private_key_size) {
    int e;
    struct nc_dh_params p;
    struct nc_dh_key x,y,X,Y;
    struct nc_key sx,sy;

    if(prime_size < 0)
        prime_size = PRIME_SIZE;
    if(private_key_size < 0)
        private_key_size = PRIVATE_KEY_SIZE;

    // generate parameters and keys

    nc_dh_params_init(&p);
    nc_dh_key_init(&x);
    nc_dh_key_init(&y);
    nc_dh_key_init(&X);
    nc_dh_key_init(&Y);

    memset(&sx, 0, sizeof(sx));
    memset(&sy, 0, sizeof(sy));

    printf("prime size = %zu bits\n", prime_size*8);
    printf("private key size = %zu bits\n", private_key_size*8);
    printf("\n");

    e = nc_dh_params_random(&p, prime_size);
    if(e != NC_OK)
        goto r1;

    gmp_printf("g (%zu bits) = \e[97m%Zu\e[0m\n", mpz_sizeinbase(p.g, 2), p.g);
    gmp_printf("p (%zu bits) = \e[97m%Zx\e[0m\n", mpz_sizeinbase(p.p, 2), p.p);
    printf("\n");

    nc_dh_key_random(&x, private_key_size, -1);
    nc_dh_key_set_params(&x,&p);
    nc_dh_key_gen_public(&x,&X); // slave

    gmp_printf("x (%zu bits) = \e[97m%Zx\e[0m\n", mpz_sizeinbase(x.v,2), x.v);
    gmp_printf("X (g^x mod p) (%zu bits) = \e[97m%Zx\e[0m\n", mpz_sizeinbase(X.v,2), X.v);
    printf("\n");

    nc_dh_key_random(&y, private_key_size, -1);
    nc_dh_key_set_params(&y,&p);
    nc_dh_key_gen_public(&y,&Y); // master

    gmp_printf("y (%zu bits) = \e[97m%Zx\e[0m\n", mpz_sizeinbase(y.v,2), y.v);
    gmp_printf("Y (g^y mod p) (%zu bits) = \e[97m%02Zx\e[0m\n", mpz_sizeinbase(Y.v,2), Y.v);
    printf("\n");

    // secret values

    nc_dh_key_gen_secret(&x, &Y, private_key_size, &sx);
    nc_dh_key_gen_secret(&y, &X, private_key_size, &sy); // master

    printf("sx (Y^x mod p) = \e[97m");
    debug_bytes(sx.buf, sx.size, false, stdout);
    printf("\e[0m\n");
    printf("sy (X^y mod p) = \e[97m");
    debug_bytes(sy.buf, sy.size, false, stdout);
    printf("\e[0m\n\n");

    if(sx.size == sy.size && memcmp(sx.buf, sy.buf, sx.size) == 0)
        printf("\e[92mdh success\e[0m\n");
    else
        printf("\e[91mdh failure\e[0m\n");

r1: nc_dh_params_deinit(&p);
    nc_dh_key_deinit(&x);
    nc_dh_key_deinit(&y);
    nc_dh_key_deinit(&X);
    nc_dh_key_deinit(&Y);
    nc_key_deinit(&sx);
    nc_key_deinit(&sy);

    return e;
}

int dsa_test(ssize_t prime_size, ssize_t private_key_size) {
    int e;
    struct nc_dsa_key x,X;
    struct nc_key hk;
    uint8_t *data,
            *hash;
    size_t len,
           i,
           sd_body_len;
    struct nc_dsa_sig sig;
    struct nc_dsa_signed_data sd;
    char *sd_tag,
         *sd_body;

    memset(&x, 0, sizeof(x));
    memset(&X, 0, sizeof(X));
    memset(&hk, 0, sizeof(hk));

    if(prime_size < 0)
        prime_size = PRIME_SIZE;
    if(private_key_size < 0)
        private_key_size = PRIVATE_KEY_SIZE;

    // generate parameters and keys

    nc_dsa_key_init(&x);
    nc_dsa_key_init(&X);

    printf("prime size = %zu bits\n", prime_size*8);
    printf("private key size = %zu bits\n", private_key_size*8);
    printf("\n");

    e = nc_dsa_key_random(&x, private_key_size, prime_size);
    if(e != NC_OK)
        goto r1;

    nc_dsa_key_gen_public(&x,&X);

    gmp_printf("x (%zu bits) = \e[97m%Zx\e[0m\n", mpz_sizeinbase(x.v,2), x.v);
    gmp_printf("X (%zu bits) = \e[97m%Zx\e[0m\n", mpz_sizeinbase(X.v,2), X.v);

    gmp_printf("p (%zu bits) = \e[97m%Zx\e[0m\n", mpz_sizeinbase(x.params.p,2), x.params.p);
    gmp_printf("q (%zu bits) = \e[97m%Zx\e[0m\n", mpz_sizeinbase(x.params.q,2), x.params.q);
    gmp_printf("g (%zu bits) = \e[97m%Zx\e[0m\n", mpz_sizeinbase(x.params.g,2), x.params.g);
    printf("\n");

    // hash

    e = nc_key_set_size(&hk, private_key_size);
    if(e != NC_OK)
        goto r1;

    data = (uint8_t *)CRYPTO_PLAINTEXT;
    len = strlen((char *)data);

    hash = malloc(hk.size);
    if(hash == NULL) {
        e = NC_MALLOC;
        goto r2;
    }

    nc_hash_auto(&hk, data, len, hash);

    printf("hash = \e[97m");
    debug_bytes(hash, hk.size, false, stdout);
    printf("\e[0m\n");

    // sign

    nc_dsa_sig_init(&sig);
    nc_dsa_sign(hash, hk.size, &x, &sig);

    gmp_printf("r = \e[97m%Zx\e[0m\n", sig.r);
    gmp_printf("s = \e[97m%Zx\e[0m\n", sig.s);
    printf("\n");

    // tag conversion

    nc_dsa_signed_data_init(&sd);

    e = nc_dsa_signed_data_set(&sd, data, len, &sig);
    if(e != NC_OK)
        goto r3;

    e = nc_dsa_signed_data_serialize(&sd, &sd_tag, NULL);
    if(e != NC_OK)
        goto r4;

    printf("%s\n", sd_tag);

    e = nc_tag_parse(sd_tag, NC_TAG_DSA_SIGNED_DATA, NULL, &sd_body, &sd_body_len, NULL);
    if(e != NC_OK)
        goto r5;

    e = nc_dsa_signed_data_deserialize(&sd, sd_body, sd_body_len);
    if(e != NC_OK)
        goto r5;

    // verify

    e = nc_dsa_signed_data_verify(&sd, &hk, &X);
    if(e == NC_OK)
        printf("\e[92msignature verified\e[0m\n");
    else
        printf("\e[91msignature invalid\e[0m\n");

    printf("\n");

r5: free(sd_tag);
r4: nc_dsa_sig_deinit(&sig);
r3: nc_dsa_signed_data_deinit(&sd);
    free(hash);
r2: nc_key_deinit(&hk);
r1: nc_dsa_key_deinit(&X);
    nc_dsa_key_deinit(&x);

    return e;
}

struct hash_try {
    char data[32];
    size_t len;
    uint8_t *hash;
};

bool hash_collision_check(struct nc_key *k, struct hash_try *hbuf, size_t n, struct hash_try *cmp, struct hash_try **hc_ret) {
    size_t i;
    struct hash_try *h;

    for(i=0; i < n; ++i) {
        h = hbuf+i;
        if(memcmp(h->hash, cmp->hash, k->size) == 0) {
            *hc_ret = h;
            return true;
        }
    }

    return false;
}

inline static
void clear_line(size_t *n, FILE *fh) {
    fwrite("\r",1,1,fh);

    while(*n > 0) {
        fwrite(" ",1,1,fh);
        --(*n);
    }

    fwrite("\r",1,1,fh);
}

int hash_test(ssize_t hash_size, ssize_t iters) {
    int e;
    struct nc_key k;
    struct hash_try *hbuf,
                    *h,
                    *c;
    size_t np,
           i,
           cc;
    uint8_t *hashes;

    if(hash_size < 0)
        hash_size = HASH_SIZE;
    if(iters < 0)
        iters = HASH_ITERS;

    memset(&k, 0, sizeof(k));

    e = nc_key_set_size(&k, hash_size);
    if(e != NC_OK)
        goto r0;

    nc_key_print_info(&k, stdout);
    printf("\n");

    hbuf = malloc(sizeof(struct hash_try)*iters);
    if(hbuf == NULL)
        return NC_MALLOC;

    memset(hbuf, 0, sizeof(struct hash_try)*iters);

    hashes = malloc(hash_size*iters);
    if(hashes == NULL)
        return NC_MALLOC;

    np = 0;
    cc = 0;

    for(i=0; i < iters; ++i) {
        h = hbuf+i;
        h->len = snprintf(h->data, 32, "%08lu", i+1);
        h->hash = hashes+(k.size*i);
        nc_hash_auto(&k, (uint8_t *)h->data, h->len, h->hash);

        if(hash_collision_check(&k, hbuf, i, h, &c)) {
            printf("  \e[91m");
            debug_bytes(h->hash, k.size, false, stdout);
            printf("\e[0m %s \e[37m=\e[0m %s\n", h->data, c->data);

            ++cc;
        }
        else {
            np += printf("> \e[96m")-5;
            np += debug_bytes(h->hash, k.size, false, stdout);
            np += printf("\e[0m %zu/%zu", i+1, iters)-4;

            fflush(stdout);
        }

        clear_line(&np, stdout);
    }

    if(cc > 0)
        printf("\n");

    printf("collisions: \e[97m%zu\e[0m/\e[97m%zu\e[0m\n", cc, iters);
    printf("variation:  \e[97m%.03f\e[0m%%\n", (((double)iters-cc)/iters)*100.0);
    printf("\n");

    e = NC_OK;

r1: nc_key_deinit(&k);
    free(hbuf);
    free(hashes);

r0: return e;
}

int crypto_test(ssize_t key_size) {
    int e;
    struct nc_key k;
    uint8_t *data,
            *hash;
    size_t len,
           pad_size,
           padded_len,
           n_blocks,
           orig_len;

    // set up key

    if(key_size < 0)
        key_size = CRYPTO_KEY_SIZE;

    memset(&k, 0, sizeof(k));

    e = nc_key_random(&k, key_size);
    if(e != NC_OK)
        return e;

    printf("\e[93m-- key --\e[0m\n");
    nc_key_print_info(&k, stdout);
    printf("\n");

    // set up main buffer

    len = strlen(CRYPTO_PLAINTEXT);
    nc_pad_size(&k, len, true, &pad_size);

    padded_len = len+pad_size;
    n_blocks = nc_block_count(&k, padded_len);

    data = malloc(padded_len);
    if(data == NULL) {
        e = NC_MALLOC;
        goto r1;
    }

    memcpy(data, CRYPTO_PLAINTEXT, len);
    memset(data+len, 0, padded_len-len);

    // pad + hash + encrypt

    printf("\e[93m-- input --\e[0m\n");
    printf("%s\n", data);
    printf("\n");

    e = nc_pad_apply(&k, data, len, pad_size, &hash);
    if(e != NC_OK)
        goto r2;

    nc_debug_pad(&k, data, len, pad_size);

    nc_key_save(&k);
    nc_encrypt_auto(&k, data, n_blocks);

    // print buffer

    printf("\e[93m-- ciphertext --\e[0m\n\n");
    nc_debug_pad(&k, data, len, pad_size);

    // decrypt + depad + verify hash

    nc_key_restore(&k);
    nc_key_advance(&k, n_blocks);
    nc_decrypt_auto(&k, data, n_blocks);

    e = nc_pad_read(&k, data, padded_len, &pad_size, &hash);
    if(e == NC_OK)
        printf("\e[92mhash verified\e[0m\n");
    else if(e == NC_BAD_HASH)
        printf("\e[91mbad hash\e[0m\n");
    else if(e != NC_OK)
        goto r2;

    printf("\n");

    // print output

    orig_len = padded_len-pad_size;

    printf("\e[93m-- output --\e[0m\n");
    fwrite(data, 1, orig_len, stdout);
    if(data[orig_len-1] != '\n')
        printf("\n");
    printf("\n");

r2: free(data);
r1: nc_key_deinit(&k);

    return e;
}

int round_test(ssize_t key_size) {
    int e;
    struct nc_key k;
    uint8_t *block;
    size_t len;

    if(key_size < 0)
        key_size = CRYPTO_KEY_SIZE;

    nc_key_init(&k);

    e = nc_key_derived_from_str(&k, -1, CRYPTO_KEY_STR, key_size);
    if(e != NC_OK)
        goto r0;

    block = malloc(k.size);
    if(block == NULL) {
        e = NC_MALLOC;
        goto r1;
    }

    len = strlen(CRYPTO_PLAINTEXT);
    if(len > k.size)
        len = k.size;

    memcpy(block, CRYPTO_PLAINTEXT, len);
    if(len < k.size)
        memset(block+len, 0, k.size-len);

    printf("\e[1;94m-- encrypt --\e[0m\n");
    printf("\n");

    printf("\e[1;93m-- input --\e[0m\n");
    printf("\e[92m");
    debug_block(&k, block, stdout);
    printf("\e[0m");

    sub_xor_bytes(&k, block);
    printf("\e[93m-- sub xor bytes --\e[0m\n");
    debug_block(&k, block, stdout);

    shift_rows(&k, block);
    printf("\e[93m-- shift rows --\e[0m\n");
    debug_block(&k, block, stdout);

    shift_columns(&k, block);
    printf("\e[93m-- shift columns --\e[0m\n");
    debug_block(&k, block, stdout);

    mix_rows(&k, block);
    printf("\e[93m-- mix rows --\e[0m\n");
    debug_block(&k, block, stdout);

    mix_columns(&k, block);
    printf("\e[93m-- mix columns --\e[0m\n");
    printf("\e[94m");
    debug_block(&k, block, stdout);
    printf("\e[0m");

    key_next(&k);
    printf("\e[93m-- key next --\e[0m\n");
    printf("\e[91m");
    debug_block(&k, k.buf, stdout);
    printf("\e[0m");

    printf("\n");
    printf("\e[1;95m-- decrypt --\e[0m\n");
    printf("\n");

    key_prev(&k);
    printf("\e[93m-- key prev --\e[0m\n");
    printf("\e[91m");
    debug_block(&k, k.buf, stdout);
    printf("\e[0m");

    mix_columns(&k, block);
    printf("\e[93m-- mix columns --\e[0m\n");
    debug_block(&k, block, stdout);

    mix_rows(&k, block);
    printf("\e[93m-- mix rows --\e[0m\n");
    debug_block(&k, block, stdout);

    unshift_columns(&k, block);
    printf("\e[93m-- unshift columns --\e[0m\n");
    debug_block(&k, block, stdout);

    unshift_rows(&k, block);
    printf("\e[93m-- unshift rows --\e[0m\n");
    debug_block(&k, block, stdout);

    rsub_xor_bytes(&k, block);
    printf("\e[93m-- output (rsub xor bytes) --\e[0m\n");

    if(memcmp(block, CRYPTO_PLAINTEXT, len) == 0)
        printf("\e[92m");
    else
        printf("\e[91m");

    debug_block(&k, block, stdout);
    printf("\e[0m");
    printf("\n");

    free(block);
r1: nc_key_deinit(&k);

r0: return e;
}

int tags_test(void) {
    int e;
    struct nc_data d1,
                   d2;
    char *data_tag,
         *key_tag,
         *dh_key_tag,
         *body;
    size_t body_len;
    struct nc_key k1,
                  k2;
    struct nc_dh_key dk1,
                     dk2;

    printf("\e[1;97mdata tag:\e[0m\n\n");

    nc_data_init(&d1);
    
    e = nc_data_set(&d1, (uint8_t *)CRYPTO_PLAINTEXT, strlen(CRYPTO_PLAINTEXT));
    if(e != NC_OK)
        goto r1;

    e = nc_data_set_meta(&d1, "tag test");
    if(e != NC_OK)
        goto r1;

    e = nc_data_serialize(&d1, &data_tag, NULL);
    if(e != NC_OK)
        goto r1;

    printf("%s\n", data_tag);

    e = nc_tag_parse(data_tag, NC_TAG_DATA, NULL, &body, &body_len, NULL);
    if(e != NC_OK)
        goto r2;

    nc_data_init(&d2);

    e = nc_data_deserialize(&d2, body, body_len);
    if(e != NC_OK)
        goto r3;

    printf("original: ");
    debug_bytes(d1.buf, d1.len, false, stdout);
    printf("\n");

    printf("reparsed: ");
    debug_bytes(d2.buf, d2.len, false, stdout);
    printf("\n\n");

    if(d2.len == d1.len && memcmp(d2.buf, d1.buf, d2.len) == 0)
        printf("\e[0;92mdata matches\e[0m\n");
    else
        printf("\e[0;91mdata does not match\e[0m\n");

    printf("\n");

    // ----

    printf("\e[1;97mkey tag:\e[0m\n\n");

    nc_key_init(&k1);

    e = nc_key_derived_from_str(&k1, PRIVATE_KEY_SIZE, CRYPTO_KEY_STR, -1);
    if(e != NC_OK)
        goto r4;

    e = nc_key_serialize(&k1, &key_tag, NULL);
    if(e != NC_OK)
        goto r4;

    printf("%s\n", key_tag);

    e = nc_tag_parse(key_tag, NC_TAG_KEY, NULL, &body, &body_len, NULL);
    if(e != NC_OK)
        goto r5;

    nc_key_init(&k2);

    e = nc_key_deserialize(&k2, body, body_len);
    if(e != NC_OK)
        goto r6;

    printf("original: ");
    debug_bytes(k1.buf, k1.size, false, stdout);
    printf("\n");

    printf("reparsed: ");
    debug_bytes(k2.buf, k2.size, false, stdout);
    printf("\n\n");

    if(k1.size == k2.size && memcmp(k1.buf, k2.buf, k1.size) == 0)
        printf("\e[0;92mkeys match\e[0m\n");
    else
        printf("\e[0;92mkeys do not match\e[0m\n");

    printf("\n");

    // ----

    printf("\e[1;97mdh key tag:\e[0m\n\n");

    nc_dh_key_init(&dk1);
    
    e = nc_dh_key_random(&dk1, PRIVATE_KEY_SIZE, PRIME_SIZE);
    if(e != NC_OK)
        goto r7;

    e = nc_dh_key_serialize(&dk1, &dh_key_tag, NULL);
    if(e != NC_OK)
        goto r7;

    printf("%s\n", dh_key_tag);

    nc_dh_key_init(&dk2);

    e = nc_tag_parse(dh_key_tag, NC_TAG_DH_KEY, NULL, &body, &body_len, NULL);
    if(e != NC_OK)
        goto r8;

    e = nc_dh_key_deserialize(&dk2, body, body_len);
    if(e != NC_OK)
        goto r8;

    gmp_printf("original: %Zx\n", dk1.v);
    gmp_printf("reparsed: %Zx\n", dk2.v);
    printf("\n");

    if(mpz_cmp(dk1.v, dk2.v) == 0)
        printf("\e[0;92mkeys match\e[0m\n");
    else
        printf("\e[0;92mkeys do not match\e[0m\n");

    printf("\n");

    // ----

    nc_dh_key_deinit(&dk2);
r8: free(dh_key_tag);
r7: nc_dh_key_deinit(&dk1);
r6: nc_key_deinit(&k2);
r5: free(key_tag);
r4: nc_key_deinit(&k1);
r3: nc_data_deinit(&d2);
r2: free(data_tag);
r1: nc_data_deinit(&d1);

r0: return e;
}

int main(int argc, char **argv) {
    int e;

    if(argc < 2)
        goto u;

    if(strcmp(argv[1], "-dh") == 0) {
        e = dh_test(argc > 2 ? strtoll(argv[2], NULL, 10) : -1,
                    argc > 3 ? strtoll(argv[3], NULL, 10) : -1);
    }
    else if(strcmp(argv[1], "-dsa") == 0) {
        e = dsa_test(argc > 2 ? strtoll(argv[2], NULL, 10) : -1,
                     argc > 3 ? strtoll(argv[3], NULL, 10) : -1);
    }
    else if(strcmp(argv[1], "-hash") == 0) {
        e = hash_test(argc > 2 ? strtoll(argv[2], NULL, 10) : -1,
                      argc > 3 ? strtoll(argv[3], NULL, 10) : -1);
    }
    else if(strcmp(argv[1], "-crypto") == 0) {
        e = crypto_test(argc > 2 ? strtoll(argv[2], NULL, 10) : -1);
    }
    else if(strcmp(argv[1], "-round") == 0) {
        e = round_test(argc > 2 ? strtoll(argv[2], NULL, 10) : -1);
    }
    else if(strcmp(argv[1], "-tags") == 0) {
        e = tags_test();
    }
    else {
    u:  fprintf(stderr, "valid arguments: -dh, -dsa, -hash, -crypto, -round, -ascii\n");
        return 1;
    }

    if(e != NC_OK)
        goto e0;

    return 0;

e0: fprintf(stderr, "\e[31merror:\e[0m \e[91m%s\e[0m\n", nc_error_str[e]);

    return e;
}
