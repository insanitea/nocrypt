#include "nocrypt.h"

#include <ctype.h>
#include <errno.h>
#include <time.h>

//
// DEBUG
//

#ifdef DEBUG

inline static
size_t debug_bytes(uint8_t *buf, size_t n, bool delimited, FILE *fh) {
    size_t np,
           i;

    if(delimited) {
        np = fprintf(fh, "%02hhx", buf[0]);
        for(i=1; i < n; ++i)
            np += fprintf(fh, " %02hhx", buf[i]);
    }
    else {
        np = 0;
        for(i=0; i < n; ++i)
            np += fprintf(fh, "%02hhx", buf[i]);
    }

    return np;
}

inline static
size_t debug_block(struct nc_key *k, uint8_t *block, FILE *fh) {
    size_t np,
           i;

    np = 0;
    for(i=0; i < k->n_rows; ++i) {
        np += debug_bytes(block, k->n_cols, true, fh);
        np += fprintf(fh, "\n");
        block += k->n_cols;
    }

    return np;
}

inline static
void debug_pad_section(struct nc_key *k, uint8_t *ptr, size_t n, size_t *row_rem, size_t *block_rem, uint8_t colour) {
    size_t rr,
           br;

    if(n == 0)
        return;

    rr = *row_rem;
    br = *block_rem;

    fprintf(stderr, "\e[%hhim", colour);

    if(n >= rr) {
        if(rr > 0) {
            // print partial row
            fprintf(stderr, " ");
            debug_bytes(ptr, rr, true, stderr);
            fprintf(stderr, "\n");

            ptr += rr;
            n -= rr;
            br -= rr;

            if(br == 0) {
                br = k->size;
                fprintf(stderr, "\n");
            }
        }

        // print whole rows
        while(n >= k->n_cols) {
            debug_bytes(ptr, k->n_cols, true, stderr);
            fprintf(stderr, "\n");

            ptr += k->n_cols;
            n -= k->n_cols;
            br -= k->n_cols;

            if(br == 0) {
                br = k->size;
                fprintf(stderr, "\n");
            }
        }

        if(n > 0) {
            // print leftover
            debug_bytes(ptr, n, true, stderr);

            br -= n;
            rr = k->n_cols-n;
        }
        else {
            rr = 0;
        }
    }
    else {
        // if (n < rr) print bytes and decrement rr
        // condition implies (rr > 0)

        fprintf(stderr, " ");
        debug_bytes(ptr, n, true, stderr);

        rr -= n;
        br -= n;
    }

    fprintf(stderr, "\e[0m");

    *row_rem = rr;
    *block_rem = br;
}

void debug_pad(struct nc_key *k, uint8_t *buf, size_t len, size_t pad_size) {
    size_t counter_size,
           alignment,
           hash_size,
           filler,
           rp,
           br;
    uint8_t *tail,
            *hash,
            *last,
            *counter;

    counter_size = pad_size/255;
    if((pad_size%255) != 0)
        ++counter_size;

    alignment = pad_size%k->size;
    hash_size = pad_size-alignment-k->size;
    filler = k->size-counter_size-1;

    tail = buf+len;
    hash = tail+alignment;
    last = hash+hash_size;
    counter = last+filler+1;

    rp=0;
    br=k->size;

    debug_pad_section(k, buf, len, &rp, &br, 97);
    debug_pad_section(k, tail, alignment, &rp, &br, 95);
    debug_pad_section(k, hash, hash_size, &rp, &br, 96);
    debug_pad_section(k, last, filler, &rp, &br, 95);
    debug_pad_section(k, counter-1, 1, &rp, &br, 37);
    debug_pad_section(k, counter, counter_size, &rp, &br, 93);

    fprintf(stderr, "data: \e[97m%zu byte(s)\e[0m\n", len);
    fprintf(stderr, "alignment: \e[95m%zu byte(s)\e[0m\n", alignment);
    fprintf(stderr, "hash: \e[96m%zu byte(s)\e[0m\n", hash_size);
    fprintf(stderr, "filler: \e[95m%zu byte(s)\e[0m\n", filler);
    fprintf(stderr, "counter: \e[93m%zu bytes\e[0m\n", counter_size+1);
    fprintf(stderr, "total: \e[97m%zu bytes\e[0m\n", pad_size);
    fprintf(stderr, "\n");
}

#define nc_debug(...) fprintf(stderr, __VA_ARGS__)
#define nc_debug_bytes(k,buf,n,dl) debug_bytes(k,buf,n,dl,stderr)
#define nc_debug_block(k,block) debug_block(k,block,stderr)
#define nc_debug_key_info(k) nc_key_print_info(k,stderr)
#define nc_debug_pad(k,buf,len,pad_size) debug_pad(k,buf,len,pad_size)

#else

#define nc_debug(...)
#define nc_debug_bytes(k,buf,n,dl)
#define nc_debug_block(k,block)
#define nc_debug_key_info(k)
#define nc_debug_pad(k,buf,len,pad_size)

#endif

//
// HEX/BIN CONVERSION
//

int from_hex(char *hex, ssize_t hex_len, uint8_t *bin_out) {
    size_t i,j;
    char byte[3],
         *end,
         *last;

    byte[2] = '\0';
    last = byte+2;

    errno = 0;
    for(i=0,j=0; i < hex_len; i+=2,++j) {
        memcpy(byte, hex+i, 2);
        bin_out[j] = strtoul(byte, &end, 16);
        if(errno != 0 || end != last)
            return NC_BAD_INPUT;
    }

    return NC_OK;
}

void to_hex(uint8_t *bin, size_t bin_len, char *hex_out) {
    void *ptr;
    size_t i,j;

    ptr = hex_out;
    for(i=0,j=0; i < bin_len; ++i,ptr+=2)
        snprintf(ptr, 3, "%02hhx", bin[i]);
}

int bin_to_hex(uint8_t *bin, size_t bin_len, char **hex_ret, size_t *hex_len_ret) {
    size_t i,j;
    size_t hex_len;
    char *hex,
         *ptr;

    hex_len = bin_len*2;

    hex = malloc(hex_len+1);
    if(hex == NULL)
        return NC_MALLOC;

    to_hex(bin, bin_len, hex);

    *hex_ret = hex;
    if(hex_len_ret != NULL)
        *hex_len_ret = hex_len;

    return NC_OK;
}

int hex_to_bin(char *hex, ssize_t hex_len, uint8_t **bin_ret, size_t *bin_len_ret) {
    int e;
    size_t bin_len,
           i,j;
    uint8_t *bin;
    char byte[3],
         *last,
         *end;

    if(hex_len < 0)
        hex_len = strlen(hex);

    bin_len = hex_len/2;

    bin = malloc(bin_len+1);
    if(bin == NULL) {
        e = NC_MALLOC;
        goto e0;
    }

    e = from_hex(hex, hex_len, bin);
    if(e != NC_OK)
        goto e1;

    bin[bin_len] = '\0';

    *bin_ret = bin;
    *bin_len_ret = bin_len;

    return NC_OK;

e1: free(bin);

e0: return e;
}

//
// RANDOM
//

#define RANDOM_TICKER_SIZE 24

#define rticker_iter(c,r,fh) { \
    r='a'+(rand()%26); \
    fwrite(&r,1,1,stderr); \
    ++c %= RANDOM_TICKER_SIZE; \
    if(c == 0) \
        fwrite("\r",1,1,fh); \
}

#define rticker_clear(c,fh) { \
    fwrite("\r",1,1,fh); \
    for(c=RANDOM_TICKER_SIZE; c > 0; --c) \
        fwrite(" ",1,1,fh); \
    fwrite("\r",1,1,fh); \
}

// 
// CRYPTO
//

const uint8_t sbox[256] = {
	0x63, 0x7C, 0x77, 0x7B, 0xF2, 0x6B, 0x6F, 0xC5, 0x30, 0x01, 0x67, 0x2B, 0xFE, 0xD7, 0xAB, 0x76,
	0xCA, 0x82, 0xC9, 0x7D, 0xFA, 0x59, 0x47, 0xF0, 0xAD, 0xD4, 0xA2, 0xAF, 0x9C, 0xA4, 0x72, 0xC0,
	0xB7, 0xFD, 0x93, 0x26, 0x36, 0x3F, 0xF7, 0xCC, 0x34, 0xA5, 0xE5, 0xF1, 0x71, 0xD8, 0x31, 0x15,
	0x04, 0xC7, 0x23, 0xC3, 0x18, 0x96, 0x05, 0x9A, 0x07, 0x12, 0x80, 0xE2, 0xEB, 0x27, 0xB2, 0x75,
	0x09, 0x83, 0x2C, 0x1A, 0x1B, 0x6E, 0x5A, 0xA0, 0x52, 0x3B, 0xD6, 0xB3, 0x29, 0xE3, 0x2F, 0x84,
	0x53, 0xD1, 0x00, 0xED, 0x20, 0xFC, 0xB1, 0x5B, 0x6A, 0xCB, 0xBE, 0x39, 0x4A, 0x4C, 0x58, 0xCF,
	0xD0, 0xEF, 0xAA, 0xFB, 0x43, 0x4D, 0x33, 0x85, 0x45, 0xF9, 0x02, 0x7F, 0x50, 0x3C, 0x9F, 0xA8,
	0x51, 0xA3, 0x40, 0x8F, 0x92, 0x9D, 0x38, 0xF5, 0xBC, 0xB6, 0xDA, 0x21, 0x10, 0xFF, 0xF3, 0xD2,
	0xCD, 0x0C, 0x13, 0xEC, 0x5F, 0x97, 0x44, 0x17, 0xC4, 0xA7, 0x7E, 0x3D, 0x64, 0x5D, 0x19, 0x73,
	0x60, 0x81, 0x4F, 0xDC, 0x22, 0x2A, 0x90, 0x88, 0x46, 0xEE, 0xB8, 0x14, 0xDE, 0x5E, 0x0B, 0xDB,
	0xE0, 0x32, 0x3A, 0x0A, 0x49, 0x06, 0x24, 0x5C, 0xC2, 0xD3, 0xAC, 0x62, 0x91, 0x95, 0xE4, 0x79,
	0xE7, 0xC8, 0x37, 0x6D, 0x8D, 0xD5, 0x4E, 0xA9, 0x6C, 0x56, 0xF4, 0xEA, 0x65, 0x7A, 0xAE, 0x08,
	0xBA, 0x78, 0x25, 0x2E, 0x1C, 0xA6, 0xB4, 0xC6, 0xE8, 0xDD, 0x74, 0x1F, 0x4B, 0xBD, 0x8B, 0x8A,
	0x70, 0x3E, 0xB5, 0x66, 0x48, 0x03, 0xF6, 0x0E, 0x61, 0x35, 0x57, 0xB9, 0x86, 0xC1, 0x1D, 0x9E,
	0xE1, 0xF8, 0x98, 0x11, 0x69, 0xD9, 0x8E, 0x94, 0x9B, 0x1E, 0x87, 0xE9, 0xCE, 0x55, 0x28, 0xDF,
	0x8C, 0xA1, 0x89, 0x0D, 0xBF, 0xE6, 0x42, 0x68, 0x41, 0x99, 0x2D, 0x0F, 0xB0, 0x54, 0xBB, 0x16
};

const uint8_t rsbox[256] = {
	0x52, 0x09, 0x6A, 0xD5, 0x30, 0x36, 0xA5, 0x38, 0xBF, 0x40, 0xA3, 0x9E, 0x81, 0xF3, 0xD7, 0xFB,
	0x7C, 0xE3, 0x39, 0x82, 0x9B, 0x2F, 0xFF, 0x87, 0x34, 0x8E, 0x43, 0x44, 0xC4, 0xDE, 0xE9, 0xCB,
	0x54, 0x7B, 0x94, 0x32, 0xA6, 0xC2, 0x23, 0x3D, 0xEE, 0x4C, 0x95, 0x0B, 0x42, 0xFA, 0xC3, 0x4E,
	0x08, 0x2E, 0xA1, 0x66, 0x28, 0xD9, 0x24, 0xB2, 0x76, 0x5B, 0xA2, 0x49, 0x6D, 0x8B, 0xD1, 0x25,
	0x72, 0xF8, 0xF6, 0x64, 0x86, 0x68, 0x98, 0x16, 0xD4, 0xA4, 0x5C, 0xCC, 0x5D, 0x65, 0xB6, 0x92,
	0x6C, 0x70, 0x48, 0x50, 0xFD, 0xED, 0xB9, 0xDA, 0x5E, 0x15, 0x46, 0x57, 0xA7, 0x8D, 0x9D, 0x84,
	0x90, 0xD8, 0xAB, 0x00, 0x8C, 0xBC, 0xD3, 0x0A, 0xF7, 0xE4, 0x58, 0x05, 0xB8, 0xB3, 0x45, 0x06,
	0xD0, 0x2C, 0x1E, 0x8F, 0xCA, 0x3F, 0x0F, 0x02, 0xC1, 0xAF, 0xBD, 0x03, 0x01, 0x13, 0x8A, 0x6B,
	0x3A, 0x91, 0x11, 0x41, 0x4F, 0x67, 0xDC, 0xEA, 0x97, 0xF2, 0xCF, 0xCE, 0xF0, 0xB4, 0xE6, 0x73,
	0x96, 0xAC, 0x74, 0x22, 0xE7, 0xAD, 0x35, 0x85, 0xE2, 0xF9, 0x37, 0xE8, 0x1C, 0x75, 0xDF, 0x6E,
	0x47, 0xF1, 0x1A, 0x71, 0x1D, 0x29, 0xC5, 0x89, 0x6F, 0xB7, 0x62, 0x0E, 0xAA, 0x18, 0xBE, 0x1B,
	0xFC, 0x56, 0x3E, 0x4B, 0xC6, 0xD2, 0x79, 0x20, 0x9A, 0xDB, 0xC0, 0xFE, 0x78, 0xCD, 0x5A, 0xF4,
	0x1F, 0xDD, 0xA8, 0x33, 0x88, 0x07, 0xC7, 0x31, 0xB1, 0x12, 0x10, 0x59, 0x27, 0x80, 0xEC, 0x5F,
	0x60, 0x51, 0x7F, 0xA9, 0x19, 0xB5, 0x4A, 0x0D, 0x2D, 0xE5, 0x7A, 0x9F, 0x93, 0xC9, 0x9C, 0xEF,
	0xA0, 0xE0, 0x3B, 0x4D, 0xAE, 0x2A, 0xF5, 0xB0, 0xC8, 0xEB, 0xBB, 0x3C, 0x83, 0x53, 0x99, 0x61,
	0x17, 0x2B, 0x04, 0x7E, 0xBA, 0x77, 0xD6, 0x26, 0xE1, 0x69, 0x14, 0x63, 0x55, 0x21, 0x0C, 0x7D
};

inline static
uint8_t rotl(uint8_t x, uint8_t c) {
    return (x << c) | (x >> (8-c));
}

inline static
uint8_t rotr(uint8_t x, uint8_t c) {
    return (x >> c) | (x << (8-c));
}

inline static
void sub_xor_bytes(struct nc_key *k, uint8_t *block) {
    size_t i;
    for(i=0; i < k->size; ++i) {
        block[i] = sbox[block[i]];
        block[i] ^= k->buf[i];
    }
}

inline static
void rsub_xor_bytes(struct nc_key *k, uint8_t *block) {
    size_t i;
    for(i=0; i < k->size; ++i) {
        block[i] ^= k->buf[i];
        block[i] = rsbox[block[i]];
    }
}

#define SHIFT_ROWS(src, dest) { \
    for(r=1; r < k->n_rows; ++r) { \
        row = block+(k->n_cols*r); \
        for(i=r%k->n_cols,j=0; j < k->n_cols; i=(i+1)%k->n_cols,++j) \
            k->tmp[dest] = row[src]; \
        memcpy(row, k->tmp, k->n_cols); \
    } \
}

#define SHIFT_COLUMNS(src, dest) { \
    for(c=1; c < k->n_cols; ++c) { \
        for(i=c%k->n_rows,j=0; j < k->n_rows; i=(i+1)%k->n_rows,++j) \
            k->tmp[dest] = block[(k->n_cols*src)+c]; \
        for(i=0; i < k->n_rows; ++i) \
            block[(k->n_cols*i)+c] = k->tmp[i]; \
    } \
}

inline static
void shift_rows(struct nc_key *k, uint8_t *block) {
    size_t r,i,j;
    uint8_t *row;
    SHIFT_ROWS(i,j);
}

inline static
void unshift_rows(struct nc_key *k, uint8_t *block) {
    size_t r,i,j;
    uint8_t *row;
    SHIFT_ROWS(j,i);
}

inline static
void shift_columns(struct nc_key *k, uint8_t *block) {
    size_t c,i,j;
    SHIFT_COLUMNS(i,j);
}

inline static
void unshift_columns(struct nc_key *k, uint8_t *block) {
    size_t c,i,j;
    SHIFT_COLUMNS(j,i);
}

inline static
void mix_rows(struct nc_key *k, uint8_t *block) {
    size_t cm,r,c1,c2;
    uint8_t *row,
            m[2];

    cm = k->n_cols/2;
    for(r=0; r < k->n_rows; ++r) {
        row = block+(k->n_cols*r);
        for(c1=0,c2=k->n_cols-1; c1 < cm; ++c1,--c2) {
            m[0] = (row[c1] & 0xAA) | (row[c2] & 0x55);
            m[1] = (row[c2] & 0xAA) | (row[c1] & 0x55);
            row[c1] = m[0];
            row[c2] = m[1];
        }
    }
}

inline static
void mix_columns(struct nc_key *k, uint8_t *block) {
    size_t rm,c,r1,r2,i,j;
    uint8_t m[2];

    rm = k->n_rows/2;
    for(c=0; c < k->n_cols; ++c) {
        for(r1=0,r2=k->n_rows-1; r1 < rm; ++r1,--r2) {
            i = (k->n_cols*r1)+c;
            j = (k->n_cols*r2)+c;
            m[0] = (block[i] & 0xAA) | (block[j] & 0x55);
            m[1] = (block[j] & 0xAA) | (block[i] & 0x55);
            block[i] = m[0];
            block[j] = m[1];
        }
    }
}

void key_next(struct nc_key *k) {
    size_t i,j;

    for(i=0,j=k->max; i < k->size; ++i,--j)
        k->buf[i] ^= sbox[k->buf[j]];

    ++k->state;
}

void key_prev(struct nc_key *k) {
    size_t i,j;

    for(i=k->max,j=0; j < k->size; --i,++j)
        k->buf[i] ^= sbox[k->buf[j]];

    --k->state;
}

//
// TAGS
//

enum {
    TPL_BIN,
    TPL_MPI,
    TPL_STR
};

struct tag_deserialize_tpl {
    char *key;
    int format;
    void *dest1,
         *dest2;
    bool required,
         loaded;
};

struct tag_serialize_tpl {
    char *key;
    int format;
    void *src;
    ssize_t src_len;
};

struct tag_data {
    char *key,
         *data;
    size_t len;
    struct tag_data *prev,
                    *next;
};

char * nc_tag_names_upper[] = {
    "DATA",
    "KEY",
    "DH KEY",
    "DH PARAMETERS",
    "DSA KEY",
    "DSA PARAMETERS",
    "DSA SIGNATURE",
    "DSA SIGNED DATA"
};

struct tag_data *
tag_data_new(char *key, char *data, size_t len) {
    int e;
    struct tag_data *x;

    x = malloc(sizeof(struct tag_data));
    if(x == NULL)
        goto e0;

    memset(x, 0, sizeof(struct tag_data));

    if(key != NULL) {
        x->key = strdup(key);
        if(x->key == NULL)
            goto e1;
    }

    x->data = data;
    x->len = len;

    return x;

e1: free(x);

e0: return NULL;
}

void tag_data_destroy(struct tag_data *x) {
    struct tag_data *xn;

    while(x != NULL) {
        xn = x->next;

        if(x->key != NULL)
            free(x->key);
        if(x->data != NULL)
            free(x->data);

        free(x);
        x = xn;
    }
}

void tag_data_detach(struct tag_data *x) {
    if(x->prev != NULL)
        x->prev->next = x->next;
    if(x->next != NULL)
        x->next->prev = x->prev;

    x->prev = NULL;
    x->next = NULL;
}

void tag_data_attach(struct tag_data *x, struct tag_data *s, int rel) {
    tag_data_detach(x);

    if(rel <= 0) {
        x->prev = s->prev;
        s->prev = x;
        x->next = s;
    }
    else {
        x->prev = s;
        x->next = s->next;
        s->next = x;
    }
}

int tag_serialize(int type, struct tag_serialize_tpl *tplv, size_t tplc, char **tag_ret, size_t *tag_len_ret) {
    int e;
    struct tag_data *head,
                    *tail,
                    *x;
    size_t i,
           data_len,
           tag_len,
           body_len,
           header_len,
           footer_len;
    uint64_t name_len;
    char *data,
         *tag,
         *ptr;

    // convert data

    head = NULL;
    tail = NULL;

    for(i=0; i < tplc; ++i) {
        if(tplv[i].src == NULL)
            continue;

        if(tplv[i].format == TPL_BIN) {
            e = bin_to_hex(tplv[i].src, tplv[i].src_len, &data, &data_len);
            if(e != NC_OK)
                goto e1;
        }
        else if(tplv[i].format == TPL_MPI) {
            e = mpi_to_str(tplv[i].src, 16, NULL, &data);
            if(e != NC_OK)
                goto e1;

            data_len = strlen(data);
        }
        else if(tplv[i].format == TPL_STR) {
            data = strdup(tplv[i].src);
            data_len = strlen(data);
        }
        else {
            continue;
        }

        x = tag_data_new(tplv[i].key, data, data_len);
        if(x == NULL) {
            e = NC_MALLOC;
            goto e2;
        }

        if(tail != NULL)
            tag_data_attach(x, tail, +1);
        else
            head = x;

        tail = x;
    }

    // calculate tag length

    tag_len = 0;
    tag_len += snprintf(NULL, 0, "-- BEGIN NOCRYPT %s --\n", nc_tag_names_upper[type]);

    for(x=head; x != NULL; x=x->next) {
        if(x->key != NULL)
            tag_len += strlen(x->key)+2; // key and ": "

        tag_len += x->len+1; // data and "\n"
    }

    tag_len += snprintf(NULL, 0, "-- END NOCRYPT %s --\n", nc_tag_names_upper[type]);

    // allocate

    tag = malloc(tag_len+1);
    if(tag == NULL) {
        e = NC_MALLOC;
        goto e1;
    }

    // build tag

    ptr = tag;
    ptr += sprintf(ptr, "-- BEGIN NOCRYPT %s --\n", nc_tag_names_upper[type]);

    for(x=head; x != NULL; x=x->next) {
        if(x->key != NULL)
            ptr += sprintf(ptr, "%s: %s\n", x->key, x->data);
        else
            ptr += sprintf(ptr, "%s\n", x->data);
    }

    ptr += sprintf(ptr, "-- END NOCRYPT %s --\n", nc_tag_names_upper[type]);

    // ----

    *tag_ret = tag;
    if(tag_len_ret != NULL)
        *tag_len_ret = tag_len;

    tag_data_destroy(head);

    return NC_OK;

e2: free(data);
e1: tag_data_destroy(head);

    return e;
}

int tag_deserialize(char *body, size_t body_len, struct tag_deserialize_tpl *tplv, size_t tplc) {
    int e;
    size_t linec,
           i,j,
           data_len,
           bin_len,
           str_len,
           *str_len_ret,
           *bin_len_ret;
    char **linev,
         *key,
         *ptr,
         *data,
         *str,
         **str_ret;
    uint8_t *bin,
            **bin_ret;

    linev = strnsplit(body, body_len, -1, "\n", &linec);
    if(linev == NULL) {
        e = NC_MALLOC;
        goto r0;
    }

    for(i=0; i < linec; ++i) {
        // extract key if any

        for(ptr=linev[i]; *ptr != '\0'; ++ptr) {
            if(*ptr == ':') {
                *(ptr++) = '\0';
                key = linev[i];
                goto c1;
            }
        }

        key = NULL;
        ptr = linev[i];
    c1: while(isspace(*ptr))
            ++ptr;

        data_len = strlen(ptr);
        data = strstrip(ptr, data_len, NULL, false, &data_len);

        for(j=0; j < tplc; ++j) {
            if((tplv[j].key == NULL && key == NULL) ||
               (tplv[j].key != NULL && key != NULL && strcmp(tplv[j].key, key) == 0))
            {
                if(tplv[j].format == TPL_BIN) {
                    e = hex_to_bin(data, data_len, &bin, &bin_len);
                    if(e != NC_OK)
                        goto r1;

                    if(tplv[j].dest1 != NULL) {
                        bin_ret = (uint8_t **)tplv[j].dest1;
                        *bin_ret = bin;

                        if(tplv[j].dest2 != NULL) {
                            bin_len_ret = (size_t *)tplv[j].dest2;
                            *bin_len_ret = bin_len;
                        }
                    }

                    tplv[j].loaded = true;
                }
                else if(tplv[j].format == TPL_MPI) {
                    e = mpi_from_str(tplv[j].dest1, 16, data, NULL);
                    if(e == MPI_MALLOC) {
                        e = NC_MALLOC;
                        goto r1;
                    }
                    else if(e != MPI_OK) {
                        e = NC_OK;
                        goto r1;
                    }

                    tplv[j].loaded = true;
                }
                else if(tplv[j].format == TPL_STR) {
                    str = strdup(data);
                    str_len = strlen(str);

                    if(tplv[j].dest1 != NULL) {
                        str_ret = (char **)tplv[j].dest1;
                        *str_ret = str;

                        if(tplv[j].dest2 != NULL) {
                            str_len_ret = (size_t *)tplv[j].dest2;
                            *str_len_ret = str_len;
                        }

                        tplv[j].loaded = true;
                    }
                }

                if(tplv[j].loaded == false && tplv[j].required) {
                    e = NC_BAD_INPUT;
                    goto r1;
                }

                break;
            }
        }
    }

    e = NC_OK;

r1: strbuf_free(linev, linec);

r0: return e;
}
