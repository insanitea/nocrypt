#include <nocrypt.h>

#include <wat/file.h>

#include <errno.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define DEFAULT_KEY_SIZE 32 // 256 bits
#define DEFAULT_PRIME_SIZE 256 // 2048 bits

enum {
    MODE_ENCRYPT,
    MODE_DECRYPT,
    MODE_HASH,
    MODE_SIGN,
    MODE_VERIFY,
    MODE_KEY_GEN,
    MODE_DH_KEY_GEN,
    MODE_DH_PARAMS_GEN,
    MODE_DSA_KEY_GEN,
    MODE_DSA_PARAMS_GEN
};

char *usage_fmt =
    "usage: nocrypt [args ..]\n"
    "\n"
    " -gk generate key\n"
    " -gd generate dh key\n"
    " -ge generate dh parameters\n"
    " -gm generate dsa key\n"
    " -gn generate dsa parameters\n"
    "\n"
    " -k <str>\n"
    "    derived key\n"
    " -kx <hex>\n"
    "    hexadecimal key\n"
    " -ks <size>\n"
    "    derived key size\n"
    " -kf <file>\n"
    "    key file\n"
    " -ykf <file>\n"
    "    dsa key file\n"
    "\n"
    " -p <size>\n"
    "    dh/dsa prime size\n"
    "    default=2048\n"
    "\n"
    " -d decrypt\n"
    " -h hash\n"
    " -s sign\n"
    " -v verify\n"
    "\n"
    " -b binary output\n"
    " -t signed data output\n"
    " -i <path>\n"
    "    input file\n"
    " -o <path>\n"
    "    output file\n"
    "\n"
    "version: %s\n"
    "https://gitlab.com/insanitea/nocrypt\n";

//
// PRINTING
//

#define print_ok(...) print_msg(2, __VA_ARGS__)
#define print_info(...) print_msg(6, __VA_ARGS__)
#define print_warning(...) print_msg(3, __VA_ARGS__)
#define print_error(...) print_msg(1, __VA_ARGS__)
#define print_nc_error(e) print_error("%s\n", nc_error_str[e])

int print_msg(unsigned int colour, char *fmt, ...) {
    char fmt2[512];
    va_list ap;
    int n;

    snprintf(fmt2, sizeof(fmt2), "\e[3%um>\e[0m \e[9%um%s\e[0m", colour, colour, fmt);

    va_start(ap, fmt);
    n = vfprintf(stderr, fmt2, ap);
    va_end(ap);

    return n;
}

//
// INPUT
//

int pre_read_data(uint8_t **data_ret, size_t *len_ret, size_t *alloc_len_ret, struct nc_dsa_sig **sig_ret, char *file) {
    int e,
        type;
    FILE *fh;
    uint8_t *data;
    char *ptr,
         *body;
    size_t len,
           alloc_len,
           body_len;
    struct nc_dsa_signed_data sd;
    struct nc_data *d;
    struct nc_dsa_sig *sig;

    if(file != NULL) {
        fh = fopen(file, "r");
        if(fh == NULL) {
            print_error("cannot open \e[97m%s\e[0m\n", file);
            goto e0;
        }

        e = freadall(&data, &len, fh);
        fclose(fh);

        alloc_len = len;
    }
    else {
        e = freadall_pooled(&data, &len, &alloc_len, stdin);
    }

    if(e == -1) {
        print_error("cannot allocate memory for input\n");

        e = NC_MALLOC;
        goto e0;
    }
    else if(e == 1) {
        if(file != NULL)
            print_error("cannot read \e[97m%s\e[0m\n", file);
        else
            print_error("cannot read \e[95mstdin\e[0m\n");

        e = NC_BAD_INPUT;
        goto e0;
    }

    d = NULL;
    sig = NULL;
    ptr = (char *)data;

    while(*ptr != '\0') {
        e = nc_tag_parse(ptr, -1, &type, &body, &body_len, &ptr);
        if(e == NC_BAD_INPUT)
            break;

        if(type == NC_TAG_DATA) {
            if(d == NULL) {
                if((e = nc_data_new(&d)) != NC_OK ||
                   (e = nc_data_deserialize(d, body, body_len)) != NC_OK)
                {
                    goto e1;
                }

                // stop if we already parsed a signature
                if(sig != NULL)
                    break;
            }
        }
        else if(type == NC_TAG_DSA_SIGNATURE) {
            if(sig == NULL) {
                if((e = nc_dsa_sig_new(&sig)) != NC_OK ||
                   (e = nc_dsa_sig_deserialize(sig, body, body_len)) != NC_OK)
                {
                    goto e1;
                }

                // stop if we already parsed the data
                if(d != NULL)
                    break;
            }
        }
        else if(type == NC_TAG_DSA_SIGNED_DATA) {
            if((e = nc_data_new(&d)) != NC_OK ||
               (e = nc_dsa_sig_new(&sig)) != NC_OK)
            {
                goto e1;
            }

            nc_dsa_signed_data_init(&sd);

            // deserialize signed data
            e = nc_dsa_signed_data_deserialize(&sd, body, body_len);
            if(e != NC_OK)
                goto e2;

            // separate the data and signature
            e = nc_dsa_signed_data_release(&sd, d, sig);
            if(e != NC_OK)
                goto e2;

            // no need to call nc_dsa_signed_data_clear() as it's now empty

            break;
        }
    }

    if(d != NULL) {
        nc_data_release(d, &data, &len, &alloc_len, NULL);
        nc_data_destroy(d);
    }

    *data_ret = data;
    *len_ret = len;
    *alloc_len_ret = alloc_len;
    *sig_ret = sig;

    return NC_OK;

e2: nc_dsa_signed_data_clear(&sd);
e1: if(d != NULL)
        nc_data_destroy(d);
    if(sig != NULL)
        nc_dsa_sig_destroy(sig);

e0: return e;
}

int pre_read_tag(int type, void *x, char *file) {
    int e;
    FILE *fh;
    void *data;
    char *tag_body;
    size_t data_len,
           tag_body_len;

    fh = fopen(file, "r");
    if(fh == NULL) {
        e = NC_OPEN;
        goto r0;
    }

    e = freadall(&data, &data_len, fh);
    if(e != 0)
        goto r1;

    e = nc_tag_parse(data, type, NULL, &tag_body, &tag_body_len, NULL);
    if(e != NC_OK)
        goto r2;

    if(type == NC_TAG_KEY) {
        e = nc_key_deserialize(x, tag_body, tag_body_len);
        if(e == NC_BAD_INPUT)
            e = nc_key_from_bin(x, data, data_len);
    }
    else if(type == NC_TAG_DH_KEY) {
        e = nc_dh_key_deserialize(x, tag_body, tag_body_len);
    }
    else if(type == NC_TAG_DH_PARAMETERS) {
        e = nc_dh_params_deserialize(x, tag_body, tag_body_len);
    }
    else if(type == NC_TAG_DSA_KEY) {
        e = nc_dsa_key_deserialize(x, tag_body, tag_body_len);
    }
    else if(type == NC_TAG_DSA_PARAMETERS) {
        e = nc_dsa_params_deserialize(x, tag_body, tag_body_len);
    }
    else {
        e = NC_BAD_INPUT;
    }

r2: free(data);
r1: fclose(fh);

r0: return e;
}

//
// OUTPUT
//

int pre_write_tag(int type, void *x, char *file) {
    int e;
    FILE *fh;
    char *tag;
    size_t tag_len;

    if(type == NC_TAG_KEY)
        e = nc_key_serialize(x, &tag, &tag_len);
    else if(type == NC_TAG_DH_KEY)
        e = nc_dh_key_serialize(x, &tag, &tag_len);
    else if(type == NC_TAG_DH_PARAMETERS)
        e = nc_dh_params_serialize(x, &tag, &tag_len);
    else if(type == NC_TAG_DSA_KEY)
        e = nc_dsa_key_serialize(x, &tag, &tag_len);
    else if(type == NC_TAG_DSA_PARAMETERS)
        e = nc_dsa_params_serialize(x, &tag, &tag_len);
    else
        e = NC_BAD_INPUT;

    if(e != NC_OK) {
        print_nc_error(e);
        goto r0;
    }

    if(file != NULL) {
        fh = fopen(file, "w");
        if(fh == NULL) {
            print_error("cannot open \e[97m%s\e[0m\n", file);
            e = -1;
            goto r1;
        }
    }
    else {
        fh = stdout;
    }

    fwrite(tag, 1, tag_len, fh);
    if(ferror(fh)) {
        print_error("cannot write %s to \e[97m%s\e[0m\n", nc_tag_names[type], file);
        e = -1;
        goto r2;
    }

    print_info("%s written to \e[97m%s\e[0m\n", nc_tag_names[type], file);
    e = NC_OK;

r2: fclose(fh);
r1: free(tag);

r0: return e;
}

int write_data(uint8_t *data, size_t len, bool binary, FILE *fh) {
    int e;
    struct nc_data d;
    char *tag;
    size_t tag_len;

    if(binary) {
        fwrite(data, 1, len, fh);
        if(ferror(fh))
            goto e0;

        fflush(fh);

        if(isatty(fileno(fh))) {
            if(data[len-1] != '\n')
                fprintf(stderr, "\n\n");
            else
                fprintf(stderr, "\n");
        }
    }
    else {
        nc_data_init(&d);
        nc_data_assign(&d, data, len, len);

        e = nc_data_serialize(&d, &tag, &tag_len);
        if(e != NC_OK)
            goto e1;

        nc_data_release(&d, NULL, NULL, NULL, NULL);
        nc_data_clear(&d);

        fwrite(tag, 1, tag_len, fh);
        if(ferror(fh))
            goto e2;

        fflush(fh);
    }

    return 0;

e2: free(tag);
e1: nc_data_release(&d, NULL, NULL, NULL, NULL);
    nc_data_clear(&d);

e0: return -1;
}

int write_hash(struct nc_key *k, uint8_t *hash, bool binary, FILE *fh) {
    size_t i;

    if(binary) {
        fwrite(hash, 1, k->size, fh);
        if(ferror(fh))
            return -1;

        fflush(fh);
    }
    else {
        for(i=0; i < k->size; ++i) {
            fprintf(fh, "%02x", hash[i]);
            if(ferror(fh))
                return -1;
        }

        fflush(fh);
        if(isatty(fileno(fh)))
            fprintf(stderr, "\n");
    }

    return 0;
}

int write_dsa_tag(int type, void *x, FILE *fh) {
    int e;
    char *tag;
    size_t tag_len;

    if(type == NC_TAG_DSA_SIGNATURE)
        e = nc_dsa_sig_serialize(x, &tag, &tag_len);
    else if(type == NC_TAG_DSA_SIGNED_DATA)
        e = nc_dsa_signed_data_serialize(x, &tag, &tag_len);
    else
        e = NC_BAD_INPUT;

    if(e != NC_OK)
        return e;

    fwrite(tag, 1, tag_len, fh);
    free(tag);

    if(ferror(fh))
        return -1;

    fflush(fh);

    return 0;
}

void auto_filename(char *buf, size_t max, char *pre_fmt, ...) {
    va_list ap;
    char pre[PATH_MAX];
    size_t i;

    va_start(ap, pre_fmt);
    vsnprintf(pre, sizeof(pre), pre_fmt, ap);
    va_end(ap);

    i = 1;
    while(true) {
        snprintf(buf, max, "%s.%03zu", pre, i);
        if(access(buf, F_OK) != 0)
            break;

        ++i;
    }
}

void auto_key_pair_filenames(char *private, size_t private_max, char *public, size_t public_max,
                             char *pre_fmt, ...)
{
    va_list ap;
    char pre[PATH_MAX];
    size_t i;

    va_start(ap, pre_fmt);
    vsnprintf(pre, sizeof(pre), pre_fmt, ap);
    va_end(ap);

    i = 1;
    while(true) {
        snprintf(private, private_max, "%s.%03zu", pre, i);
        snprintf(public, public_max, "%s.%03zu.pub", pre, i);

        if(access(private, F_OK) != 0 ||
           access(public, F_OK) != 0)
        {
            break;
        }

        ++i;
    }
}

int auto_write_data(uint8_t *data, size_t len, bool binary_output, bool decrypted) {
    int e;
    char file[PATH_MAX];
    FILE *fh;

    auto_filename(file, PATH_MAX, decrypted ? "output" : "output.nc");

    fh = fopen(file, "w");
    if(fh == NULL) {
        print_error("cannot open output file\n");
        goto e0;
    }

    e = write_data(data, len, binary_output, fh);
    if(e == -1) {
        print_error("cannot convert data to tag\n");
        goto e1;
    }
    else if(e == 1) {
        print_error("cannot write to output file\n");
        goto e1;
    }

    fprintf(stderr, "output written to \e[97m%s\e[0m\n", file);
    fclose(fh);

    return 0;

e1: fclose(fh);

e0: return -1;
}

//
// HELPERS
//

int bin_to_hash(struct nc_key *k, uint8_t *data, size_t len, uint8_t **hash_ret) {
    int e;
    uint8_t *hash;

    hash = malloc(nc_key_get_size(k));
    if(hash == NULL) {
        print_nc_error(e);
        return NC_MALLOC;
    }

    nc_hash_auto(k, data, len, hash);
    *hash_ret = hash;

    return NC_OK;
}

//
// MAGIC
//

int main(int argc, char **argv) {
    int e,r;
    size_t i,j;
    FILE *fh,
         *input,
         *output;
    struct nc_key k;
    struct nc_dh_key dhk1,
                     dhk2;
    struct nc_dh_params dhp;
    struct nc_dsa_key dsak1,
                      dsak2;
    struct nc_dsa_params dsap;
    struct nc_dsa_sig *sig;
    struct nc_dsa_signed_data sd;
    struct nc_data *d;
    uint8_t *data,
            *tmp,
            *hash;
    char *strval,
         *end,
         private_file[PATH_MAX],
         public_file[PATH_MAX];
    size_t len,
           alloc_len,
           tag_len,
           alignment,
           pad_size,
           padded_len,
           n_blocks;

    if(argc == 1) {
        fprintf(stderr, usage_fmt, nc_version_str);
        goto e0;
    }

    // process arguments

    int mode = MODE_ENCRYPT;
    bool binary_output = false,
         signed_data_output = false;
    char *key_str = NULL,
         *key_hex = NULL,
         *key_file = NULL,
         *dh_key_file = NULL,
         *dsa_key_file = NULL,
         *input_file = NULL,
         *output_file = NULL;
    ssize_t key_size = -1,
            prime_gen_size = -1;

#define FAIL_NO_ARG(error_msg) \
    if(j >= argc) { \
        print_error(error_msg); \
        goto e0; \
    }

#define FAIL_INVALID_INT_ARG(error_msg) \
    if(errno != 0 || *end != '\0') { \
        print_error(error_msg); \
        goto e0; \
    }

    for(i=1; i < argc; i=j) {
        j = i+1;

        // generate key
        if(strcmp(argv[i], "-gk") == 0) {
            mode = MODE_KEY_GEN;
        }
        // generate dh key
        else if(strcmp(argv[i], "-gxk") == 0) {
            mode = MODE_DH_KEY_GEN;
        }
        // generate dh params
        else if(strcmp(argv[i], "-gxp") == 0) {
            mode = MODE_DH_PARAMS_GEN;
        }
        // generate dsa key
        else if(strcmp(argv[i], "-gyk") == 0) {
            mode = MODE_DSA_KEY_GEN;
        }
        //  dsa params
        else if(strcmp(argv[i], "-gyp") == 0) {
            mode = MODE_DSA_PARAMS_GEN;
        }
        // key string
        else if(strcmp(argv[i], "-k") == 0) {
            FAIL_NO_ARG("no key specified\n");
            key_str = argv[j++];
        }
        // hex key
        else if(strcmp(argv[i], "-kx") == 0) {
            FAIL_NO_ARG("no key specified\n");
            key_hex = argv[j++];
        }
        // key size
        else if(strcmp(argv[i], "-ks") == 0) {
            FAIL_NO_ARG("no key size specified\n");

            strval = argv[j++];
            errno = 0;
            key_size = strtoull(strval, &end, 10);
            FAIL_INVALID_INT_ARG("invalid key size specified\n");

            // bits to bytes
            key_size /= 8;
        }
        // key file
        else if(strcmp(argv[i], "-kf") == 0) {
            FAIL_NO_ARG("no key file specified\n");
            key_file = argv[j++];
        }
        // dh key file
        else if(strcmp(argv[i], "-xkf") == 0) {
            FAIL_NO_ARG("no dh key file specified\n");
            dh_key_file = argv[j++];
        }
        // dsa key file
        else if(strcmp(argv[i], "-ykf") == 0) {
            FAIL_NO_ARG("no dsa key file specified\n");
            dsa_key_file = argv[j++];
        }
        // dh/dsa prime size
        else if(strcmp(argv[i], "-p") == 0) {
            FAIL_NO_ARG("no prime size specified\n");

            strval = argv[j++];
            errno = 0;
            prime_gen_size = strtoull(strval, &end, 10);
            FAIL_INVALID_INT_ARG("invalid prime size specified\n");

            // bits to bytes
            prime_gen_size /= 8;
        }
        // decrypt
        else if(strcmp(argv[i], "-d") == 0) {
            mode = MODE_DECRYPT;
        }
        // hash
        else if(strcmp(argv[i], "-h") == 0) {
            mode = MODE_HASH;
        }
        // dsa sign
        else if(strcmp(argv[i], "-s") == 0) {
            mode = MODE_SIGN;
        }
        // dsa verify
        else if(strcmp(argv[i], "-v") == 0) {
            mode = MODE_VERIFY;
        }
        // signed data output
        else if(strcmp(argv[i], "-t") == 0) {
            signed_data_output = true;
        }
        // binary output
        else if(strcmp(argv[i], "-b") == 0) {
            binary_output = true;
        }
        // input file
        else if(strcmp(argv[i], "-i") == 0) {
            FAIL_NO_ARG("no input file specified\n");
            input_file = argv[j++];
        }
        // output file
        else if(strcmp(argv[i], "-o") == 0) {
            FAIL_NO_ARG("no output file specified\n");
            output_file = argv[j++];
        }
    }

    // key/paramater generation

    if(mode == MODE_KEY_GEN) {
        memset(&k, 0, sizeof(struct nc_key));

        if(key_str != NULL) {
            // derived key
            e = nc_key_derived_from_str(&k, key_size, key_str, -1);
        }
        else if(key_hex != NULL) {
            // hex-derived key
            e = nc_key_derived_from_hex(&k, key_size, key_hex, len);
        }
        else {
            if(key_size == -1)
                key_size = DEFAULT_KEY_SIZE;

            // random key
            e = nc_key_random(&k, key_size);
        }

        if(e != NC_OK) {
            print_nc_error(e);
            goto e0;
        }

        nc_key_print_info(&k, stderr);
        fprintf(stderr, "\n");

        if(output_file == NULL) {
            output_file = alloca(PATH_MAX);
            auto_filename(output_file, PATH_MAX, "nc-key-%zu", k.size*8);
        }

        e = pre_write_tag(NC_TAG_KEY, &k, output_file);
        nc_key_clear(&k);

        if(e != NC_OK)
            goto e0;

        return 0;
    }
    else if(mode == MODE_DH_KEY_GEN) {
        if(key_size == -1)
            key_size = DEFAULT_KEY_SIZE;
        if(prime_gen_size == -1)
            prime_gen_size = DEFAULT_PRIME_SIZE;

        nc_dh_key_init(&dhk1);
        nc_dh_key_init(&dhk2);

        e = nc_dh_key_random(&dhk1, key_size, prime_gen_size);
        if(e != NC_OK) {
            print_nc_error(e);
            goto x1;
        }

        nc_dh_key_gen_public(&dhk1, &dhk2);
        auto_key_pair_filenames(private_file, sizeof(private_file),
                                public_file, sizeof(public_file),
                                "nc-dhk-%zu-%zu", key_size*8, prime_gen_size*8);

        if((e = pre_write_tag(NC_TAG_DH_KEY, &dhk1, private_file)) != NC_OK ||
           (e = pre_write_tag(NC_TAG_DH_KEY, &dhk2, public_file)) != NC_OK)
        {
            goto x1;
        }

        nc_dh_key_clear(&dhk1);
        nc_dh_key_clear(&dhk2);

        return 0;
    }
    else if(mode == MODE_DH_PARAMS_GEN) {
        if(prime_gen_size == -1)
            prime_gen_size = DEFAULT_PRIME_SIZE;

        nc_dh_params_init(&dhp);
        nc_dh_params_random(&dhp, prime_gen_size);

        if(output_file == NULL) {
            output_file = alloca(PATH_MAX);
            auto_filename(output_file, PATH_MAX, "nc-dhp-%zu", prime_gen_size*8);
        }

        e = pre_write_tag(NC_TAG_DH_PARAMETERS, &dhp, output_file);
        nc_dh_params_clear(&dhp);

        if(e != 0)
            goto e0;

        return 0;
    }
    else if(mode == MODE_DSA_KEY_GEN) {
        if(key_size == -1)
            key_size = DEFAULT_KEY_SIZE;
        if(prime_gen_size == -1)
            prime_gen_size = DEFAULT_PRIME_SIZE;

        nc_dsa_key_init(&dsak1);
        nc_dsa_key_init(&dsak2);

        e = nc_dsa_key_random(&dsak1, key_size, prime_gen_size);
        if(e != NC_OK) {
            print_nc_error(e);
            goto y2;
        }

        nc_dsa_key_gen_public(&dsak1, &dsak2);
        auto_key_pair_filenames(private_file, sizeof(private_file),
                                public_file, sizeof(public_file),
                                "nc-dsak-%zu-%zu", key_size*8, prime_gen_size*8);

        if((e = pre_write_tag(NC_TAG_DSA_KEY, &dsak1, private_file)) != NC_OK ||
           (e = pre_write_tag(NC_TAG_DSA_KEY, &dsak2, public_file)) != NC_OK)
        {
            goto y2;
        }

        nc_dsa_key_clear(&dsak1);
        nc_dsa_key_clear(&dsak2);

        return 0;
    }
    else if(mode == MODE_DSA_PARAMS_GEN) {
        if(prime_gen_size == -1)
            prime_gen_size = DEFAULT_PRIME_SIZE;

        nc_dsa_params_init(&dsap);
        nc_dsa_params_random(&dsap, prime_gen_size);

        if(output_file == NULL) {
            output_file = alloca(PATH_MAX);
            auto_filename(output_file, PATH_MAX, "nc-dsap-%zu", prime_gen_size*8);
        }

        e = pre_write_tag(NC_TAG_DSA_PARAMETERS, &dsap, output_file);
        nc_dsa_params_clear(&dsap);

        if(e != 0)
            goto e0;

        return 0;
    }

    // set up symmetric key

    nc_key_init(&k);

    if(key_file != NULL) {
        e = pre_read_tag(NC_TAG_KEY, &k, key_file);
    }
    else if(key_str != NULL) {
        e = nc_key_derived_from_str(&k, key_size, key_str, -1);
    }
    else if(key_hex != NULL) {
        if(key_size >= 0) {
            // hex-derived
            e = nc_key_derived_from_hex(&k, key_size, key_hex, -1);
        }
        else {
            // raw hex
            e = nc_key_from_hex(&k, key_hex, -1);
        }
    }
    else if(key_size >= 0 && (mode == MODE_HASH ||
                              mode == MODE_SIGN ||
                              mode == MODE_VERIFY))
    {
        // use zeroed key for hashing
        e = nc_key_set_size(&k, key_size);
    }
    else {
        print_error("no key specified\n");
        goto e0;
    }

    if(e != NC_OK) {
        print_nc_error(e);
        goto e0;
    }

    // read input

    e = pre_read_data(&data, &len, &alloc_len, &sig, input_file);
    if(e != NC_OK)
        goto e1;

    // prepare output stream

    if(output_file != NULL) {
        output = fopen(output_file, "w");
        if(output == NULL) {
            print_error("cannot open output file (\e[97m%s\e[0m)\n", output_file);
            goto e2;
        }
    }
    else {
        output = stdout;
    }

    // fun stuff

    r = 0;

    if(mode == MODE_ENCRYPT) {
        nc_pad_size(&k, len, true, &pad_size);

        e = nc_pad_prealloc(&data, len, &alloc_len, pad_size);
        if(e != NC_OK) {
            print_nc_error(e);
            goto e3;
        }

        e = nc_pad_apply(&k, data, len, pad_size, &hash);
        if(e != NC_OK) {
            print_nc_error(e);
            goto e3;
        }

        padded_len = len+pad_size;
        n_blocks = nc_block_count(&k, padded_len);
        nc_encrypt_auto(&k, data, n_blocks);

        e = write_data(data, padded_len, binary_output, output);
        if(e == -1)
            goto e3;
    }
    else if(mode == MODE_DECRYPT) {
        padded_len = len;
        n_blocks = nc_block_count(&k, padded_len);

        nc_key_advance(&k, n_blocks);
        nc_decrypt_auto(&k, data, n_blocks);

        e = nc_pad_read(&k, data, padded_len, &pad_size, &hash);
        if(e == NC_BAD_HASH) {
            // not fatal but ciphertext was tampered with
            print_warning("bad hash");
        }
        else if(e != NC_OK) {
            print_nc_error(e);
            goto e3;
        }

        len -= pad_size;

        e = write_data(data, len, true, output);
        if(e == -1)
            goto e3;
    }
    else if(mode == MODE_HASH) {
        hash = malloc(nc_key_get_size(&k));
        if(hash == NULL) {
            print_nc_error(e);
            goto e3;
        }

        nc_hash_auto(&k, data, len, hash);

        e = write_hash(&k, hash, binary_output, output);
        free(hash);

        if(e == -1)
            goto e3;
    }
    else if(mode == MODE_SIGN) {
        if(dsa_key_file == NULL) {
            print_error("no dsa key specified\n");
            goto e3;
        }

        nc_dsa_key_init(&dsak1);

        e = pre_read_tag(NC_TAG_DSA_KEY, &dsak1, dsa_key_file);
        if(e != NC_OK) {
            print_nc_error(e);
            goto h1;
        }

        if(sig != NULL)
            nc_dsa_sig_destroy(sig);

        e = nc_dsa_sig_new(&sig);
        if(e != NC_OK)
            goto h1;

        e = bin_to_hash(&k, data, len, &hash);
        if(e != NC_OK)
            goto h1;

        nc_dsa_sign(hash, k.size, &dsak1, sig);

        if(signed_data_output) {
            nc_dsa_signed_data_init(&sd);
            nc_dsa_signed_data_assign(&sd, data, len, alloc_len, sig);

            e = write_dsa_tag(NC_TAG_DSA_SIGNED_DATA, &sd, output);

            nc_dsa_signed_data_release(&sd, NULL, NULL);
            nc_dsa_signed_data_clear(&sd);

            if(e != NC_OK)
                goto y1;
        }
        else {
            e = write_dsa_tag(NC_TAG_DSA_SIGNATURE, sig, output);
            if(e != NC_OK)
                goto y1;
        }

        nc_dsa_key_clear(&dsak1);
        free(hash);
    }
    else if(mode == MODE_VERIFY) {
        if(dsa_key_file == NULL) {
            print_error("no dsa key specified\n");
            goto e3;
        }

        if(sig == NULL) {
            print_error("no signature to verify\n");
            goto e3;
        }

        nc_dsa_key_init(&dsak1);

        e = pre_read_tag(NC_TAG_DSA_KEY, &dsak1, dsa_key_file);
        if(e != NC_OK) {
            print_nc_error(e);
            goto y1;
        }

        e = bin_to_hash(&k, data, len, &hash);
        if(e != NC_OK)
            goto y1;

        e = nc_dsa_verify(sig, hash, k.size, &dsak1);
        if(e == NC_BAD_SIGNATURE) {
            r = 1;
            print_error("bad signature\n");
        }
        else {
            print_ok("signature verified\n");
        }

        nc_dsa_key_clear(&dsak1);
        free(hash);
    }

    // cleanup

    nc_key_clear(&k);
    free(data);
    if(sig != NULL)
        nc_dsa_sig_destroy(sig);

    if(output_file != NULL) {
        fclose(output);
        print_info("output written to \e[97m%s\e[0m\n", output_file);
    }

    return r;

    // cleanup

    // diffie hellman
x1: nc_dh_key_clear(&dhk1);
    nc_dh_key_clear(&dhk2);
    goto e3;

    // dsa
y2: nc_dsa_key_clear(&dsak2);
y1: nc_dsa_key_clear(&dsak1);
    goto e3;

    // hashing
h1: free(hash);
    goto e3;

    // general
e3: if(output_file != NULL)
        fclose(output);
e2: free(data);
    if(sig != NULL)
        nc_dsa_sig_destroy(sig);
e1: nc_key_clear(&k);

e0: return 1;
}
