#ifndef NOCRYPT_H
#define NOCRYPT_H

#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <sys/types.h>

#include <wat/mpi.h>

#define NC_HASH_Z_INIT 0xAA
#define NC_KEY_MIN_SIZE 2
#define NC_KEY_MULTIPLE 2

enum nc_error_code {
    NC_OK,
    NC_MALLOC,
    NC_OPEN,
    NC_READ,
    NC_RANDOM,
    NC_BAD_INPUT,
    NC_BAD_KEY_SIZE,
    NC_BAD_HASH,
    NC_BAD_SIGNATURE,
//  --
    NC_ERROR_MAX
};

enum {
    NC_TAG_DATA,
    NC_TAG_KEY,
    NC_TAG_DH_KEY,
    NC_TAG_DH_PARAMETERS,
    NC_TAG_DSA_KEY,
    NC_TAG_DSA_PARAMETERS,
    NC_TAG_DSA_SIGNATURE,
    NC_TAG_DSA_SIGNED_DATA,
//  ----
    NC_TAG_MAX
};

struct nc_data {
    uint8_t *buf;
    size_t len,
           alloc_len;
    char *meta;
};

struct nc_key {
	uint8_t *buf,
            *save,
            *tmp;
    size_t size,
           max,
           n_rows,
           n_cols,
           n_rounds;
    uintmax_t state,
              save_state;
    char *meta;
};

struct nc_dh_params {
    mpi_t g,p;
    char *meta;
};

struct nc_dh_key {
    mpi_t v;
    struct nc_dh_params params;
    char *meta;
};

struct nc_dsa_params {
    mpi_t g,p,q;
    char *meta;
};

struct nc_dsa_key {
    mpi_t v;
    struct nc_dsa_params params;
    char *meta;
};

struct nc_dsa_sig {
    mpi_t r,s;
    char *meta;
};

struct nc_dsa_signed_data {
    struct nc_data data;
    struct nc_dsa_sig sig;
    char *meta;
};

extern char *nc_error_str[NC_ERROR_MAX];
extern char *nc_tag_names[NC_TAG_MAX];
extern char *nc_version_str;

// RANDOM
// -------

int nc_random(void *buf, size_t n);
int nc_prime_random(bool safe, size_t size, mpi_t **p_ret);

// TAGS
// ------

int nc_tag_parse(char *ptr, int expected_type,
                 int *type_ret, char **body_ret, size_t *len_ret, char **ptr_ret);

// GENERAL
// --------

int  nc_data_new(struct nc_data **d_ret);
void nc_data_destroy(struct nc_data *d);
void nc_data_init(struct nc_data *d);
void nc_data_clear(struct nc_data *d);
int  nc_data_realloc(struct nc_data *d, size_t alloc_len);
void nc_data_assign(struct nc_data *d, uint8_t *data, size_t len, size_t alloc_len);
void nc_data_release(struct nc_data *d, uint8_t **data_ret, size_t *len_ret, size_t *alloc_len_ret, char **meta_ret);
int  nc_data_set(struct nc_data *d, uint8_t *data, size_t len);
int  nc_data_set_meta(struct nc_data *d, char *meta);
void nc_data_get(struct nc_data *d, uint8_t **data_ret, size_t *len_ret, size_t *alloc_len_ret);
void nc_data_get_meta(struct nc_data *d, char **meta_ret);
int  nc_data_deserialize(struct nc_data *d, char *body, size_t len);
int  nc_data_serialize(struct nc_data *d, char **tag_ret, size_t *tag_len_ret);

size_t nc_block_count(struct nc_key *k, size_t len);
int    nc_alloc_block(struct nc_key *k, uint8_t **block_ret);
int    nc_alloc_super_block(struct nc_key *k, uint8_t **sblock_ret);

// KEYS
// -----

#define nc_key_init(k) memset(k, 0, sizeof(struct nc_key))
#define nc_key_size_from_len(len) (((len+(NC_KEY_MULTIPLE-1))/NC_KEY_MULTIPLE)*NC_KEY_MULTIPLE)

int    nc_key_new(struct nc_key **k_ret);
void   nc_key_destroy(struct nc_key *k);
void   nc_key_clear(struct nc_key *k);
int    nc_key_copy(struct nc_key *k, struct nc_key *src);
int    nc_key_set_meta(struct nc_key *k, char *meta);
int    nc_key_set_size(struct nc_key *k, size_t size);
size_t nc_key_get_size(struct nc_key *k);
void   nc_key_get_grid_size(struct nc_key *k, size_t *rows_ret, size_t *cols_ret);
size_t nc_key_get_rounds(struct nc_key *k);
int    nc_key_random(struct nc_key *k, size_t size);
int    nc_key_derived(struct nc_key *k, ssize_t size, uint8_t *bin, size_t bin_len);
int    nc_key_derived_from_hex(struct nc_key *k, ssize_t size, char *hex, ssize_t hex_len);
int    nc_key_derived_from_str(struct nc_key *k, ssize_t size, char *str, ssize_t str_len);
int    nc_key_from_file(struct nc_key *k, char *file);
int    nc_key_from_bin(struct nc_key *k, uint8_t *bin, size_t bin_len);
int    nc_key_from_hex(struct nc_key *k, char *hex, ssize_t hex_len);
int    nc_key_deserialize(struct nc_key *k, char *body, size_t body_len);
int    nc_key_serialize(struct nc_key *k, char **tag_ret, size_t *tag_len_ret);
void   nc_key_advance(struct nc_key *k, size_t n_blocks);
void   nc_key_save(struct nc_key *k);
void   nc_key_restore(struct nc_key *k);
void   nc_key_print_info(struct nc_key *k, FILE *fh);

// PADDING
// --------

void nc_pad_size(struct nc_key *k, size_t data_len, bool with_hash, size_t *pad_size_ret);
int nc_pad_prealloc(uint8_t **data_ptr, size_t data_len, size_t *alloc_data_len_ptr, size_t pad_size);
int nc_pad_apply(struct nc_key *k, uint8_t *data, size_t data_len, size_t pad_size, uint8_t **hash_ret);
int nc_pad_read(struct nc_key *k, uint8_t *data, size_t padded_len, size_t *pad_size_ret, uint8_t **hash_ret);

// CRYPTO
// -------

size_t nc_block_count(struct nc_key *k, size_t data_len);

#define nc_hash_init(z) (z=NC_HASH_Z_INIT)
void nc_hash(struct nc_key *k, uint8_t *z, uint8_t *block, uint8_t *hash_out);
void nc_hash_auto(struct nc_key *k, uint8_t *data, size_t data_len, uint8_t *hash_out);
int  nc_hash_verify(struct nc_key *k, uint8_t *hash, uint8_t *data, size_t data_len);

void nc_encrypt(struct nc_key *k, uint8_t *block);
void nc_encrypt_auto(struct nc_key *k, uint8_t *data, size_t n_blocks);
void nc_decrypt(struct nc_key *k, uint8_t *block);
void nc_decrypt_auto(struct nc_key *k, uint8_t *data, size_t n_blocks);

// DIFFIE-HELLMAN
// ---------------

int  nc_dh_key_init(struct nc_dh_key *k);
void nc_dh_key_clear(struct nc_dh_key *k);
int  nc_dh_key_copy(struct nc_dh_key *k, struct nc_dh_key *src);
int  nc_dh_key_set_params(struct nc_dh_key *k, struct nc_dh_params *params);
void nc_dh_key_get_params(struct nc_dh_key *k, struct nc_dh_params **params_ret);
int  nc_dh_key_set_meta(struct nc_dh_key *k, char *meta);
int  nc_dh_key_random(struct nc_dh_key *k, ssize_t private_size, ssize_t prime_size);
int  nc_dh_key_from_bin(struct nc_dh_key *k, void *v, size_t v_size,
                                             void *g, size_t g_size,
                                             void *p, size_t p_size);
int  nc_dh_key_to_bin(struct nc_dh_key *k, void **v_ret, size_t *v_size_ret,
                                           void **g_ret, size_t *g_size_ret,
                                           void **p_ret, size_t *p_size_ret);
int  nc_dh_key_from_hex(struct nc_dh_key *k, char *v, char *g, char *p);
int  nc_dh_key_to_hex(struct nc_dh_key *k, char **v_ret, char **g_ret, char **p_ret);
int  nc_dh_key_deserialize(struct nc_dh_key *k, char *body, size_t body_len);
int  nc_dh_key_serialize(struct nc_dh_key *k, char **tag_ret, size_t *tag_len_ret);
int  nc_dh_key_gen_public(struct nc_dh_key *x, struct nc_dh_key *X);
int  nc_dh_key_gen_secret(struct nc_dh_key *x, struct nc_dh_key *Y, ssize_t size, struct nc_key *s);

int  nc_dh_params_init(struct nc_dh_params *params);
void nc_dh_params_clear(struct nc_dh_params *params);
int  nc_dh_params_copy(struct nc_dh_params *params, struct nc_dh_params *src);
int  nc_dh_params_set_meta(struct nc_dh_params *params, char *meta);
int  nc_dh_params_random(struct nc_dh_params *params, size_t prime_size);
int  nc_dh_params_from_bin(struct nc_dh_params *params, void *g, size_t g_len,
                                                        void *p, size_t p_len);
int  nc_dh_params_to_bin(struct nc_dh_params *params, void **g_ret, size_t *g_len_ret,
                                                      void **p_ret, size_t *p_len_ret);
int  nc_dh_params_from_hex(struct nc_dh_params *params, char *g, char *p);
int  nc_dh_params_to_hex(struct nc_dh_params *params, char **g_ret, char **p_ret);
int  nc_dh_params_deserialize(struct nc_dh_params *params, char *body, size_t body_len);
int  nc_dh_params_serialize(struct nc_dh_params *params, char **tag_ret, size_t *tag_len_ret);

// DSA
// ----

int  nc_dsa_key_init(struct nc_dsa_key *k);
void nc_dsa_key_clear(struct nc_dsa_key *k);
int  nc_dsa_key_copy(struct nc_dsa_key *k, struct nc_dsa_key *src);
int  nc_dsa_key_set_meta(struct nc_dsa_key *k, char *meta);
int  nc_dsa_key_random(struct nc_dsa_key *k, ssize_t private_size, ssize_t prime_size);
int  nc_dsa_key_gen_public(struct nc_dsa_key *x, struct nc_dsa_key *X);
int  nc_dsa_key_set_params(struct nc_dsa_key *k, struct nc_dsa_params *params);
void nc_dsa_key_get_params(struct nc_dsa_key *k, struct nc_dsa_params **params_ret);
int  nc_dsa_key_from_bin(struct nc_dsa_key *k, void *v, size_t v_len,
                                               void *g, size_t g_len,
                                               void *p, size_t p_len,
                                               void *q, size_t q_len);
int  nc_dsa_key_to_bin(struct nc_dsa_key *k, void **v_ret, size_t *v_len_ret,
                                             void **g_ret, size_t *g_len_ret,
                                             void **p_ret, size_t *p_len_ret,
                                             void **q_ret, size_t *q_len_ret);
int  nc_dsa_key_from_hex(struct nc_dsa_key *k, char *v, char *g, char *p, char *q);
int  nc_dsa_key_to_hex(struct nc_dsa_key *k, char **v_ret, char **g_ret, char **p_ret, char **q_ret);
int  nc_dsa_key_deserialize(struct nc_dsa_key *k, char *body, size_t body_len);
int  nc_dsa_key_serialize(struct nc_dsa_key *k, char **tag_ret, size_t *tag_len_ret);

int  nc_dsa_params_init(struct nc_dsa_params *params);
void nc_dsa_params_clear(struct nc_dsa_params *params);
int  nc_dsa_params_copy(struct nc_dsa_params *params, struct nc_dsa_params *src);
int  nc_dsa_params_random(struct nc_dsa_params *params, size_t prime_size);
int  nc_dsa_params_from_bin(struct nc_dsa_params *params, void *g, size_t g_len,
                                                          void *p, size_t p_len,
                                                          void *q, size_t q_len);
int  nc_dsa_params_to_bin(struct nc_dsa_params *params, void **g_ret, size_t *g_len_ret,
                                                        void **p_ret, size_t *p_len_ret,
                                                        void **q_ret, size_t *q_len_ret);
int  nc_dsa_params_from_hex(struct nc_dsa_params *params, char *g, char *p, char *q);
int  nc_dsa_params_to_hex(struct nc_dsa_params *params, char **g_ret, char **p_ret, char **q_ret);
int  nc_dsa_params_deserialize(struct nc_dsa_params *params, char *body, size_t body_len);
int  nc_dsa_params_serialize(struct nc_dsa_params *params, char **tag_ret, size_t *tag_len_ret);

int  nc_dsa_sign(uint8_t *hash, size_t hash_size, struct nc_dsa_key *x, struct nc_dsa_sig *sig);
int  nc_dsa_verify(struct nc_dsa_sig *sig, uint8_t *hash, size_t hash_size, struct nc_dsa_key *Y);

int  nc_dsa_sig_new(struct nc_dsa_sig **sig_ret);
void nc_dsa_sig_destroy(struct nc_dsa_sig *sig);
int  nc_dsa_sig_init(struct nc_dsa_sig *sig);
void nc_dsa_sig_clear(struct nc_dsa_sig *sig);
int  nc_dsa_sig_copy(struct nc_dsa_sig *sig, struct nc_dsa_sig *src);
int  nc_dsa_sig_set_meta(struct nc_dsa_sig *sig, char *meta);
int  nc_dsa_sig_from_bin(struct nc_dsa_sig **sig_ret, void *r_bin, size_t r_bin_len,
                                                      void *s_bin, size_t s_bin_len);
int  nc_dsa_sig_to_bin(struct nc_dsa_sig *sig, void **r_bin_ret, size_t *r_bin_len_ret,
                                               void **s_bin_ret, size_t *s_bin_len_ret);
int  nc_dsa_sig_from_hex(struct nc_dsa_sig **sig_ret, char *r_hex, char *s_hex);
int  nc_dsa_sig_to_hex(struct nc_dsa_sig *sig, char **r_ret, char **s_ret);
int  nc_dsa_sig_deserialize(struct nc_dsa_sig *sig, char *body, size_t body_len);
int  nc_dsa_sig_serialize(struct nc_dsa_sig *sig, char **tag_ret, size_t *tag_len_ret);

int  nc_dsa_signed_data_new(struct nc_dsa_signed_data **sd_ret);
void nc_dsa_signed_data_destroy(struct nc_dsa_signed_data *sd);
void nc_dsa_signed_data_init(struct nc_dsa_signed_data *sd);
void nc_dsa_signed_data_clear(struct nc_dsa_signed_data *sd);
int  nc_dsa_signed_data_assign(struct nc_dsa_signed_data *sd, uint8_t *data, size_t len, size_t alloc_len, struct nc_dsa_sig *sig);
int  nc_dsa_signed_data_release(struct nc_dsa_signed_data *sd, struct nc_data *data_out, struct nc_dsa_sig *sig_out);
int  nc_dsa_signed_data_set(struct nc_dsa_signed_data *sd, uint8_t *data, size_t len, struct nc_dsa_sig *sig);
int  nc_dsa_signed_data_set_meta(struct nc_dsa_signed_data *sd, char *meta);
void nc_dsa_signed_data_get(struct nc_dsa_signed_data *sd, struct nc_data **data_ret);
int  nc_dsa_signed_data_deserialize(struct nc_dsa_signed_data *sd, char *body, size_t body_len);
int  nc_dsa_signed_data_serialize(struct nc_dsa_signed_data *sd, char **tag_ret, size_t *tag_len_ret);
void nc_dsa_signed_data_hash(struct nc_dsa_signed_data *sd, struct nc_key *hk, uint8_t *hash_out);
int  nc_dsa_signed_data_verify(struct nc_dsa_signed_data *sd, struct nc_key *hk, struct nc_dsa_key *Y);

#endif
