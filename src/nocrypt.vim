function NcPrepKey()
    if exists('b:nc_key') && !empty(b:nc_key)
        return 1
    elseif !empty($NC_KEY_FILE)
        let b:nc_key = $NC_KEY_FILE
        let b:nc_key_flag = "-kf"
        return 1
    else
        call inputsave()
        let msg = printf("password for %s: ", expand('%:p'))
        let b:nc_key = inputsecret(msg)
        let b:nc_key_flag = "-k"
        unlet msg
        call inputrestore()
        return !empty(b:nc_key)
    endif
endfunction

function NcEncrypt()
    if !NcPrepKey()
        return
    endif

    silent! execute '%!nocrypt -a' b:nc_key_flag b:nc_key '2>/dev/null'
endfunction

function NcRestorePlain()
    silent! undo
    set nomod
endfunction

function NcDecrypt()
    if !NcPrepKey()
        return
    endif

    silent! execute '%!nocrypt -d -a' b:nc_key_flag b:nc_key '2>/dev/null'

    if v:shell_error == 0
        set nomod
    else
        silent! undo
        unlet b:nc_key
    endif
endfunction

augroup Nocrypt
    au!
    au BufWritePre *.nc call NcEncrypt()
    au BufWritePost *.nc call NcRestorePlain()
    au BufReadPost *.nc call NcDecrypt()
augroup END
