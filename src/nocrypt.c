#include "nocrypt.h"

#include <math.h>
#include <stdlib.h>
#include <sys/random.h>
#include <wat/file.h>
#include <wat/mpi.h>
#include <wat/string.h>

#include "internal.c"

char * nc_error_str[] = {
    "ok",
    "malloc() failed",
    "fopen() failed",
    "read() failed",
    "getrandom() failed",
    "bad input",
    "bad key size",
    "bad hash",
    "bad signature"
};

char * nc_tag_names[] = {
    "data",
    "key",
    "dh key",
    "dh parameters",
    "dsa key",
    "dsa parameters",
    "dsa signature",
    "dsa signed data"
};

char *nc_version_str = NC_VERSION;

// TAGS
// -----

int nc_tag_parse(char *ptr, int expected_type,
                 int *type_ret, char **body_ret, size_t *body_len_ret, char **ptr_ret)
{
    int e,
        type;
    size_t n,i,
           footer_len,
           body_len;
    char footer[64],
         *body;

    while(isspace(*ptr))
        ++ptr;

    // match tag header

    e = strscancmp(ptr, "-- BEGIN NOCRYPT ", &n);
    if(e != 0)
        return NC_BAD_INPUT;

    ptr += n;

    for(i=0; i < NC_TAG_MAX; ++i) {
        if(strscancmp(ptr, nc_tag_names_upper[i], &n) == 0) {
            ptr += n;

            if(strscancmp(ptr, " --\n", &n) != 0)
                return NC_BAD_INPUT;

            ptr += n;

            if(expected_type < 0 || i == expected_type) {
                type = i;
                goto c1;
            }

            break;
        }
    }

    // skip any whitespace

c1: while(isspace(*ptr))
        ++ptr;

    // match tag footer

    footer_len = snprintf(footer, sizeof(footer), "\n-- END NOCRYPT %s --", nc_tag_names_upper[type]);

    for(i=0; ptr[i] != '\0'; ++i) {
        if(strncmp(ptr+i, footer, footer_len) == 0) {
            body = ptr;
            body_len = i;

            ptr += body_len;
            ptr += footer_len;

            goto c2;
        }
    }

    return NC_BAD_INPUT;

c2: *body_ret = body;
    *body_len_ret = body_len;

    if(type_ret != NULL)
        *type_ret = type;
    if(ptr_ret != NULL)
        *ptr_ret = ptr;

    return NC_OK;
}

// GENERAL
// --------

int nc_data_new(struct nc_data **d_ret) {
    struct nc_data *d;

    d = malloc(sizeof(struct nc_data));
    if(d == NULL)
        return NC_MALLOC;

    nc_data_init(d);

    *d_ret = d;

    return NC_OK;
}

void nc_data_destroy(struct nc_data *d) {
    nc_data_clear(d);
    free(d);
}

void nc_data_init(struct nc_data *d) {
    memset(d, 0, sizeof(struct nc_data));
}

void nc_data_clear(struct nc_data *d) {
    if(d->buf != NULL)
        free(d->buf);
    if(d->meta != NULL)
        free(d->meta);

    memset(d, 0, sizeof(struct nc_data));
}

int nc_data_realloc(struct nc_data *d, size_t alloc_len) {
    uint8_t *tmp;

    tmp = realloc(d->buf, alloc_len);
    if(tmp == NULL)
        return NC_MALLOC;

    if(alloc_len > d->alloc_len)
        memset(tmp+d->alloc_len, 0, alloc_len-d->alloc_len);

    d->buf = tmp;
    d->alloc_len = alloc_len;

    return NC_OK;
}

void nc_data_assign(struct nc_data *d, uint8_t *data, size_t len, size_t alloc_len) {
    if(d->buf != NULL)
        free(d->buf);

    d->buf = data;
    d->len = len;
    d->alloc_len = alloc_len;
}

void nc_data_assign_from(struct nc_data *dest, struct nc_data *src) {
    if(dest->buf != NULL)
        free(dest->buf);

    dest->buf = src->buf;
    dest->len = src->len;
    dest->alloc_len = src->alloc_len;

    src->buf = NULL;
    src->len = 0;
    src->alloc_len = 0;
}

void nc_data_release(struct nc_data *d, uint8_t **data_ret, size_t *len_ret, size_t *alloc_len_ret, char **meta_ret) {
    nc_data_get(d, data_ret, len_ret, alloc_len_ret);

    if(meta_ret != NULL) {
        nc_data_get_meta(d, meta_ret);
        free(d->meta);
    }

    memset(d, 0, sizeof(struct nc_data));
}

int nc_data_set(struct nc_data *d, uint8_t *data, size_t len) {
    int e;

    if(data != NULL) {
        if(len > d->alloc_len) {
            e = nc_data_realloc(d, len);
            if(e != NC_OK)
                return e;
        }

        memcpy(d->buf, data, len);
        if(d->len > len)
            memset(d->buf+len, 0, d->len-len);

        d->len = len;
    }
    else {
        nc_data_clear(d);
    }

    return NC_OK;
}

int nc_data_set_meta(struct nc_data *d, char *meta) {
    char *tmp;

    if(meta != NULL) {
        tmp = strdup(meta);
        if(tmp == NULL)
            return NC_MALLOC;
    }
    else {
        tmp = NULL;
    }

    if(d->meta != NULL)
        free(d->meta);

    d->meta = tmp;

    return NC_OK;
}

void nc_data_get(struct nc_data *d, uint8_t **data_ret, size_t *len_ret, size_t *alloc_len_ret) {
    if(data_ret != NULL)
        *data_ret = d->buf;
    if(len_ret != NULL)
        *len_ret = d->len;
    if(alloc_len_ret != NULL)
        *alloc_len_ret = d->alloc_len;
}

void nc_data_get_meta(struct nc_data *d, char **meta_ret) {
    *meta_ret = d->meta;
}

int nc_data_deserialize(struct nc_data *d, char *body, size_t len) {
    nc_data_clear(d);

    struct tag_deserialize_tpl tplv[] = {
        { NULL,      TPL_BIN, &d->buf,  &d->len, true  },
        { "meta",    TPL_STR, &d->meta, NULL,    false },
    //  { "version", TPL_STR, &version, NULL,    false }
    };

    return tag_deserialize(body, len, tplv, sizeof(tplv)/sizeof(tplv[0]));
}

int nc_data_serialize(struct nc_data *d, char **tag_ret, size_t *len_ret) {
    char version[64];

    snprintf(version, sizeof(version), "nocrypt %s", nc_version_str);

    struct tag_serialize_tpl tplv[] = {
        { NULL,      TPL_BIN, d->buf,  d->len },
        { "meta",    TPL_STR, d->meta, -1     },
        { "version", TPL_STR, version, -1     }
    };

    return tag_serialize(NC_TAG_DATA, tplv, sizeof(tplv)/sizeof(tplv[0]), tag_ret, len_ret);
}

// KEYS
// -----

int nc_key_new(struct nc_key **k_ret) {
    struct nc_key *k;

    k = malloc(sizeof(struct nc_key));
    if(k == NULL)
        return NC_MALLOC;

    nc_key_init(k);

    *k_ret = k;

    return NC_OK;
}

void nc_key_destroy(struct nc_key *k) {
    nc_key_clear(k);
    free(k);
}

void nc_key_clear(struct nc_key *k) {
    if(k->buf != NULL)
        free(k->buf);
    if(k->meta != NULL)
        free(k->meta);

    memset(k, 0, sizeof(struct nc_key));
}

int nc_key_copy(struct nc_key *k, struct nc_key *src) {
    int e;

    e = nc_key_set_size(k, src->size);
    if(e != NC_OK)
        return e;

    memcpy(k->buf, src->buf, src->size);
    memcpy(k->save, src->save, src->size);
    memcpy(k->tmp, src->tmp, src->size);

    k->n_rows = src->n_rows;
    k->n_cols = src->n_cols;
    k->n_rounds = src->n_rounds;

    k->state = src->state;
    k->save_state = src->save_state;

    return NC_OK;
}

int nc_key_set_meta(struct nc_key *k, char *meta) {
    char *tmp;

    if(meta != NULL) {
        tmp = strdup(meta);
        if(tmp == NULL)
            return NC_MALLOC;
    }
    else {
        tmp = NULL;
    }

    if(k->meta != NULL)
        free(k->meta);

    k->meta = tmp;

    return NC_OK;
}

int nc_key_set_size(struct nc_key *k, size_t size) {
    size_t ksize_x3;
    uint8_t *buf;

    if(size < NC_KEY_MIN_SIZE || (size % NC_KEY_MULTIPLE) != 0)
        return NC_BAD_KEY_SIZE;

    // allocate new buffer

    ksize_x3 = size*3;

    buf = malloc(ksize_x3);
    if(buf == NULL)
        return NC_MALLOC;

    memset(buf, 0, ksize_x3);

    // free old buffer

    if(k->buf != NULL) {
        free(k->buf);
        k->buf = NULL;
    }

    // finalize

    k->buf = buf;
    k->save = k->buf+size;
    k->tmp = k->buf+(size*2);

    k->size = size;
    k->max = size-1;

    k->n_rows = floor(sqrt(size));
    while(size % k->n_rows)
        --k->n_rows;

    k->n_cols = size/k->n_rows;
    k->n_rounds = log2(size);

    k->state = 0;
    k->save_state = -1;

    return NC_OK;
}

size_t nc_key_get_size(struct nc_key *k) {
    return k->size;
}

void nc_key_get_grid_size(struct nc_key *k, size_t *rows_ret, size_t *cols_ret) {
    *rows_ret = k->n_rows;
    *cols_ret = k->n_cols;
}

size_t nc_key_get_rounds(struct nc_key *k) {
    return k->n_rounds;
}

int nc_key_random(struct nc_key *k, size_t size) {
    int e;
    size_t nr;

    e = nc_key_set_size(k, size);
    if(e != NC_OK)
        return e;

    nr = getrandom(k->buf, k->size, 0);
    if(nr < size)
        return NC_RANDOM;

	return NC_OK;
}

int nc_key_derived(struct nc_key *k, ssize_t size, uint8_t *bin, size_t bin_len) {
    int e;

    if(size < 0)
        size = nc_key_size_from_len(bin_len);

    e = nc_key_set_size(k, size);
    if(e != NC_OK)
        return e;

    nc_hash_auto(k, bin, bin_len, k->buf);

    return NC_OK;
}

int nc_key_derived_from_hex(struct nc_key *k, ssize_t size, char *hex, ssize_t hex_len) {
    int e;
    size_t bin_len;
    uint8_t *bin;

    if(hex_len < 0)
        hex_len = strlen(hex);

    if(hex_len % 2) {
        e = NC_BAD_INPUT;
        goto e0;
    }

    e = hex_to_bin(hex, hex_len, &bin, &bin_len);
    if(e != NC_OK)
        goto e1;

    e = nc_key_derived(k, size, bin, bin_len);
    if(e != NC_OK)
        goto e1;

    free(bin);

    return NC_OK;

e1: free(bin);

e0: return e;
}

int nc_key_derived_from_str(struct nc_key *k, ssize_t size, char *str, ssize_t str_len) {
    int e;
    
    if(str_len < 0)
        str_len = strlen(str);

    e = nc_key_derived(k, size, (uint8_t *)str, str_len);
    if(e != NC_OK)
        return e;

    return NC_OK;
}

int nc_key_from_bin(struct nc_key *k, uint8_t *bin, size_t bin_len) {
    int e;

    e = nc_key_set_size(k, bin_len);
    if(e != NC_OK)
        return e;

    memcpy(k->buf, bin, bin_len);

    return NC_OK;
}

int nc_key_from_hex(struct nc_key *k, char *hex, ssize_t hex_len) {
    int e;

    if(hex_len < 0)
        hex_len = strlen(hex);

    if(hex_len % NC_KEY_MULTIPLE)
        return NC_BAD_INPUT;

    e = nc_key_set_size(k, hex_len/2);
    if(e != NC_OK)
        return e;

    e = from_hex(hex, hex_len, k->buf);
    if(e != NC_OK)
        return e;

    return NC_OK;
}

int nc_key_deserialize(struct nc_key *k, char *body, size_t len) {
    int e;
    uint8_t *bin;
    size_t bin_len;

    nc_key_clear(k);

    struct tag_deserialize_tpl tplv[] = {
        { NULL,      TPL_BIN, &bin,     &bin_len, true  },
        { "meta",    TPL_STR, &k->meta, NULL,     false },
    //  { "version", TPL_STR, &version, NULL,     false }
    };

    e = tag_deserialize(body, len, tplv, sizeof(tplv)/sizeof(tplv[0]));
    if(e != NC_OK)
        goto r0;

    e = nc_key_from_bin(k, bin, bin_len);
    if(e != NC_OK)
        goto r1;

r1: free(bin);

r0: return e;
}

int nc_key_serialize(struct nc_key *k, char **tag_ret, size_t *len_ret) {
    char version[64];

    snprintf(version, sizeof(version), "nocrypt %s", nc_version_str);

    struct tag_serialize_tpl tplv[] = {
        { NULL,      TPL_BIN, k->buf,  k->size },
        { "meta",    TPL_STR, k->meta, -1      },
        { "version", TPL_STR, version, -1      }
    };

    return tag_serialize(NC_TAG_KEY, tplv, sizeof(tplv)/sizeof(tplv[0]), tag_ret, len_ret);
}

void nc_key_advance(struct nc_key *k, size_t n_blocks) {
    size_t b,r;

    for(b=0; b < n_blocks; ++b) {
        for(r=0; r < k->n_rounds; ++r)
            key_next(k);
    }
}

void nc_key_save(struct nc_key *k) {
    memcpy(k->save, k->buf, k->size);
    k->save_state = k->state;
}

void nc_key_restore(struct nc_key *k) {
    memcpy(k->buf, k->save, k->size);
    k->state = k->save_state;
}

void nc_key_print_info(struct nc_key *k, FILE *fh) {
    fprintf(fh, "size: \e[97m%zu bytes (%zu bits)\e[0m\n", k->size, k->size*8);
    fprintf(fh, "block: \e[97m%zux%zu\e[0m\n", k->n_rows, k->n_cols);
    fprintf(fh, "rounds: \e[97m%zu\e[0m\n", k->n_rounds);
}

// PADDING
// --------

void nc_pad_size(struct nc_key *k, size_t data_len, bool with_hash, size_t *pad_size_ret) {
    size_t rem,
           alignment,
           pad_size;

    // create pad layout for k relative to data_len

    rem = data_len%k->size;
    alignment = rem ? k->size-rem : 0;
    pad_size = alignment + (with_hash ? (k->size*2) : k->size);

    *pad_size_ret = pad_size;
}

int nc_pad_prealloc(uint8_t **data_ptr, size_t data_len, size_t *alloc_data_len_ptr, size_t pad_size) {
    int e;
    uint8_t *data,
            *tmp;
    size_t padded_data_len,
           alloc_data_len;

    data = *data_ptr;
    padded_data_len = data_len+pad_size;

    if(alloc_data_len_ptr != NULL) {
        alloc_data_len = *alloc_data_len_ptr;

        if(alloc_data_len < padded_data_len) {
            tmp = realloc(data, padded_data_len);
            if(tmp == NULL)
                return NC_MALLOC;

            data = tmp;
            alloc_data_len = padded_data_len;

            memset(data+data_len, 0, alloc_data_len-data_len);

            *data_ptr = data;
            *alloc_data_len_ptr = alloc_data_len;
        }
    }

    return NC_OK;
}

int nc_pad_apply(struct nc_key *k, uint8_t *data, size_t data_len, size_t pad_size, uint8_t **hash_ret) {
    int e;
    uint8_t *tail,
            *hash,
            *last,
            *counter;
    size_t alignment,
           aux_count,
           counter_mod,
           counter_size,
           filler,
           i;
    ssize_t nr;
    bool with_hash;

    // randomize alignment

    tail = data+data_len;
    alignment = pad_size%k->size;

    nr = getrandom(tail, alignment, 0);
    if(nr < alignment)
        return NC_RANDOM;

    // set up pointers and fill hash (if any)

    aux_count = (pad_size-alignment-k->size);

    if(aux_count == k->size) {
        hash = tail+alignment;
        last = hash+k->size;
        nc_hash_auto(k, data, data_len, hash);
    }
    else if(aux_count == 0) {
        hash = NULL;
        last = tail+alignment;
    }
    else {
        return NC_BAD_INPUT;
    }

    *hash_ret = hash;

    // randomize unused counter bytes

    counter_mod = pad_size%UINT8_MAX;
    counter_size = pad_size/UINT8_MAX;
    if(counter_mod != 0)
        ++counter_size;

    filler = k->size-counter_size-1;
    if(filler > 0) {
        nr = getrandom(last, filler, 0);
        if(nr < filler)
            return NC_RANDOM;
    }

    // fill counter

    counter = last+filler+1;

    counter[-1] = 0;
    counter[0] = counter_mod;
    for(i=1; i < counter_size; ++i)
        counter[i] = UINT8_MAX;

    // buffer contents are now:
    // [ data  alignment ] [ hash ] [ filler  0x00  counter ]

    return NC_OK;
}

int nc_pad_read(struct nc_key *k, uint8_t *data, size_t padded_len, size_t *pad_size_ret, uint8_t **hash_ret) {
    int e;
    size_t i,j,
           pad_size,
           orig_len,
           alignment,
           aux_count;
    uint8_t *tail,
            *hash,
            *last;

    if(padded_len%k->size != 0)
        return NC_BAD_INPUT;

    pad_size = 0;

    for(i=0; i < padded_len; ++i) {
        j = padded_len-1-i;
        if(data[j] == 0)
            break;

        pad_size += data[j];
    }

    if(pad_size < k->size || pad_size > padded_len)
        return NC_BAD_INPUT;

    orig_len = padded_len-pad_size;
    alignment = pad_size%k->size;
    aux_count = pad_size-alignment-k->size;

    tail = data+orig_len;
    if(aux_count == k->size) {
        hash = tail+alignment;
        last = hash+k->size;
    }
    else if(aux_count == 0) {
        hash = NULL;
        last = tail+alignment;
    }
    else {
        return NC_BAD_INPUT;
    }

    if(hash != NULL) {
        e = nc_hash_verify(k, hash, data, orig_len);
        if(e != NC_OK)
            return e;
    }

    *pad_size_ret = pad_size;
    if(hash_ret != NULL)
        *hash_ret = hash;

    return NC_OK;
}

// CRYPTO
// -------

size_t nc_block_count(struct nc_key *k, size_t data_len) {
    return data_len/k->size;
}

inline
void nc_hash(struct nc_key *k, uint8_t *z, uint8_t *block, uint8_t *hash_out) {
    size_t r,i;

    for(r=0; r < k->n_rounds; ++r) {
        for(i=0; i < k->size; ++i) {
            *z += k->buf[i];
            hash_out[i] ^= sbox[block[i]^(*z)];
            *z += hash_out[i];
        }

        shift_rows(k, hash_out);
        shift_columns(k, hash_out);
        mix_rows(k, hash_out);
        mix_columns(k, hash_out);
    }
}

void nc_hash_auto(struct nc_key *k, uint8_t *data, size_t data_len, uint8_t *hash_out) {
    int e;
    uint8_t z;
    size_t n_blocks,
           rem,
           b,
           i;
    uint8_t *block,
            *tail;

    nc_hash_init(z);
    memset(hash_out, 0, k->size);

    if(data_len == 0) {
        n_blocks = 0;
        rem = 0;
        goto r1;
    }

    n_blocks = data_len/k->size;
    rem = data_len%k->size;

    block = data;
    for(b=0; b < n_blocks; ++b) {
        nc_hash(k, &z, block, hash_out);
        block += k->size;
    }

    if(rem > 0) {
    r1: tail = block;
        block = k->tmp;

        memcpy(block, tail, rem);
        memset(block+rem, 0, k->size-rem);

        nc_hash(k, &z, block, hash_out);
    }
}

int nc_hash_verify(struct nc_key *k, uint8_t *hash, uint8_t *data, size_t data_len) {
    int e;
    uint8_t *vhash;

    vhash = malloc(k->size);
    if(vhash == NULL)
        return NC_MALLOC;

    nc_hash_auto(k, data, data_len, vhash);
    if(memcmp(vhash, hash, k->size) == 0)
        e = NC_OK;
    else
        e = NC_BAD_HASH;

    free(vhash);
    return e;
}

inline
void nc_encrypt(struct nc_key *k, uint8_t *block) {
    size_t r;

    for(r=0; r < k->n_rounds; ++r) {
        shift_rows(k, block);
        shift_columns(k, block);
        mix_rows(k, block);
        mix_columns(k, block);
        sub_xor_bytes(k, block);
        key_next(k);
    }
}

void nc_encrypt_auto(struct nc_key *k, uint8_t *data, size_t n_blocks) {
    uint8_t *block;
    size_t b;

    block = data;
    for(b=0; b < n_blocks; ++b) {
        nc_encrypt(k, block);
        block += k->size;
    }
}

int nc_encrypt_stream(struct nc_key *k, int fd_in, int fd_out, bool with_hash, size_t len) {
    int e;
    uint8_t *tmp,
            *hash;
    size_t n,
           alignment,
           pad_size;
    ssize_t nr,
            nw;

    tmp = malloc(k->size*3);
    if(tmp == NULL)
        goto r0;

    n = len;
    while(n > 0) {
        nr = read(fd_in, tmp, k->size);
        if(nr < 0) {
            e = NC_READ;
            goto r1;
        }

        if(nr == 0)
            break;

        n -= nr;

        if(nr < k->size) {
            nc_pad_size(k, nr, with_hash, &pad_size);
            nc_pad_apply(k, tmp, nr, pad_size, NULL);
            nc_encrypt(k, tmp);

            write(fd_out, tmp, k->size);

            break;
        }
        else {
            nc_encrypt(k, tmp);
        }

        nw = write(fd_out, tmp, k->size);
        if(nw < k->size)
            break;
    }

    e = NC_OK;

r1: free(tmp);
r0: return e;
}

inline
void nc_decrypt(struct nc_key *k, uint8_t *block) {
    size_t r;

    for(r=0; r < k->n_rounds; ++r) {
        key_prev(k);
        rsub_xor_bytes(k, block);
        mix_columns(k, block);
        mix_rows(k, block);
        unshift_columns(k, block);
        unshift_rows(k, block);
    }
}

void nc_decrypt_auto(struct nc_key *k, uint8_t *data, size_t n_blocks) {
    uint8_t *block;
    size_t b;

    block = data+(k->size*(n_blocks-1));
    for(b=n_blocks; b > 0; --b) {
        nc_decrypt(k, block);
        block -= k->size;
    }
}

int nc_decrypt_stream(struct nc_key *k, int fd, size_t n_blocks) {
    // TODO

    return NC_OK;
}

// DIFFIE-HELLMAN
// ---------------

int nc_dh_key_init(struct nc_dh_key *k) {
    int e;

    e = mpi_init(&k->v);
    if(e != MPI_OK)
        return NC_MALLOC;

    e = nc_dh_params_init(&k->params);
    if(e != MPI_OK)
        return e;

    k->meta = NULL;

    return NC_OK;
}

void nc_dh_key_clear(struct nc_dh_key *k) {
    mpi_clear(&k->v);
    nc_dh_params_clear(&k->params);
    if(k->meta != NULL) {
        free(k->meta);
        k->meta = NULL;
    }
}

int nc_dh_key_copy(struct nc_dh_key *k, struct nc_dh_key *src) {
    int e;

    e = mpi_copy(&k->v, &src->v);
    if(e != MPI_OK)
        return NC_MALLOC;

    return nc_dh_params_copy(&k->params, &src->params);
}

int nc_dh_key_set_params(struct nc_dh_key *k, struct nc_dh_params *params) {
    if(mpi_copy(&k->params.g, &params->g) != MPI_OK ||
       mpi_copy(&k->params.p, &params->p) != MPI_OK)
    {
        return NC_MALLOC;
    }

    return NC_OK;
}

void nc_dh_key_get_params(struct nc_dh_key *k, struct nc_dh_params **params_ret) {
    *params_ret = &k->params;
}

int nc_dh_key_set_meta(struct nc_dh_key *k, char *meta) {
    char *tmp;

    if(meta != NULL) {
        tmp = strdup(meta);
        if(tmp == NULL)
            return NC_MALLOC;
    }
    else {
        tmp = NULL;
    }

    if(k->meta != NULL)
        free(k->meta);

    k->meta = tmp;

    return NC_OK;
}

int nc_dh_key_random(struct nc_dh_key *k, ssize_t private_size, ssize_t prime_size) {
    int e;

    if(private_size >= 0) {
        e = mpi_random(&k->v, private_size, MPI_NONZERO);
        if(e != NC_OK)
            return e;
    }

    if(prime_size >= 0) {
        e = nc_dh_params_random(&k->params, prime_size);
        if(e != NC_OK)
            return e;
    }

    return NC_OK;
}

int nc_dh_key_from_hex(struct nc_dh_key *k, char *v, char *g, char *p) {
    if((v != NULL && mpi_from_str(&k->v, 16, v, NULL) != MPI_OK) ||
       (g != NULL && mpi_from_str(&k->params.g, 16, g, NULL) != MPI_OK) ||
       (p != NULL && mpi_from_str(&k->params.p, 16, p, NULL) != MPI_OK))
    {
        return NC_BAD_INPUT;
    }

    return NC_OK;
}

int nc_dh_key_to_hex(struct nc_dh_key *k, char **v_ret, char **g_ret, char **p_ret) {
    int e;
    char *v,*g,*p;

    if(v_ret != NULL) {
        e = mpi_to_str(&k->v, 16, NULL, &v);
        if(e != NC_OK)
            goto e0;

        *v_ret = v;
    }

    if(g_ret != NULL) {
        e = mpi_to_str(&k->params.g, 16, NULL, &g);
        if(e != NC_OK)
            goto e1;

        *g_ret = g;
    }

    if(p_ret != NULL) {
        e = mpi_to_str(&k->params.p, 16, NULL, &p);
        if(e != NC_OK)
            goto e2;

        *p_ret = p;
    }

    return NC_OK;

e2: free(g);
e1: free(v);

e0: return e;
}

int nc_dh_key_from_bin(struct nc_dh_key *k, void *v, size_t v_len,
                                            void *g, size_t g_len,
                                            void *p, size_t p_len)
{
    int e;

    if(v != NULL) {
        e = mpi_import(&k->v, v, v_len, 0, 1, 0);
        if(e != MPI_OK)
            return NC_MALLOC;
    }

    return nc_dh_params_from_bin(&k->params, g, g_len, p, p_len);
}

int nc_dh_key_to_bin(struct nc_dh_key *k, void **v_ret, size_t *v_len_ret,
                                          void **g_ret, size_t *g_size_ret,
                                          void **p_ret, size_t *p_size_ret)
{
    int e;
    size_t v_len;
    void *v;

    if(v_ret != NULL) {
        v_len = mpi_byte_size(&k->v);

        v = malloc(v_len);
        if(v == NULL) {
            e = NC_MALLOC;
            goto e0;
        }

        e = mpi_export(&k->v, v, v_len, 0, 1, 0);
        if(e != NC_OK)
            goto e1;

        *v_ret = v;
        *v_len_ret = v_len;
    }

    e = nc_dh_params_to_bin(&k->params, g_ret, g_size_ret, p_ret, p_size_ret);
    if(e != NC_OK)
        goto e1;

    return NC_OK;

e1: free(v);

e0: return e;
}

int nc_dh_key_deserialize(struct nc_dh_key *k, char *body, size_t len) {
    struct tag_deserialize_tpl tplv[] = {
        { NULL,      TPL_MPI, &k->v,        NULL, true  },
        { "g",       TPL_MPI, &k->params.g, NULL, true  },
        { "p",       TPL_MPI, &k->params.p, NULL, true  },
        { "meta",    TPL_STR, &k->meta,     NULL, false },
    //  { "version", TPL_STR, &version,     NULL, false }
    };

    return tag_deserialize(body, len, tplv, sizeof(tplv)/sizeof(tplv[0]));
}

int nc_dh_key_serialize(struct nc_dh_key *k, char **tag_ret, size_t *len_ret) {
    char version[64];

    snprintf(version, sizeof(version), "nocrypt %s", nc_version_str);

    struct tag_serialize_tpl tplv[] = {
        { NULL,      TPL_MPI, &k->v,        -1 },
        { "g",       TPL_MPI, &k->params.g, -1 },
        { "p",       TPL_MPI, &k->params.p, -1 },
        { "meta",    TPL_STR, k->meta,      -1 },
        { "version", TPL_STR, version,      -1 }
    };

    return tag_serialize(NC_TAG_DH_KEY, tplv, sizeof(tplv)/sizeof(tplv[0]), tag_ret, len_ret);
}

int nc_dh_key_gen_public(struct nc_dh_key *x, struct nc_dh_key *X) {
    int e;

    e = mpi_powm(&X->v, &x->params.g, &x->v, &x->params.p);
    if(e == MPI_MALLOC)
        return NC_MALLOC;
    else if(e != MPI_OK)
        return NC_BAD_INPUT;

    if(mpi_copy(&X->params.g, &x->params.g) != MPI_OK ||
       mpi_copy(&X->params.p, &x->params.p) != MPI_OK)
    {
        return NC_MALLOC;
    }

    return NC_OK;
}

int nc_dh_key_gen_secret(struct nc_dh_key *x, struct nc_dh_key *Y, ssize_t size, struct nc_key *sk) { 
    int e;
    mpi_t s;
    size_t n_bits,
           sv_size;
    uint8_t *sv;

    e = mpi_init(&s);
    if(e != MPI_OK)
        return NC_MALLOC;

    e = mpi_powm(&s, &Y->v, &x->v, &x->params.p);
    if(e == MPI_MALLOC)
        return NC_MALLOC;
    else if(e != MPI_OK)
        return NC_BAD_INPUT;

    sv_size = mpi_byte_size(&s);

    sv = malloc(sv_size);
    if(sv == NULL) {
        e = NC_MALLOC;
        goto e1;
    }

    e = mpi_export(&s, sv, sv_size, 0, 1, 0);
    if(e != MPI_OK)
        goto e1;

    mpi_clear(&s);

    e = nc_key_derived(sk, size, sv, sv_size);
    if(e != NC_OK)
        goto e2;

    free(sv);

    return NC_OK;

e2: free(sv);
e1: mpi_clear(&s);

    return e;
}

int nc_dh_params_init(struct nc_dh_params *params) {
    int e;

    e = mpi_init_multi(&params->g, &params->p, NULL);
    if(e != MPI_OK)
        return NC_MALLOC;

    params->meta = NULL;

    return NC_OK;
}

void nc_dh_params_clear(struct nc_dh_params *params) {
    mpi_clear_multi(&params->g, &params->p, NULL);
    if(params->meta != NULL) {
        free(params->meta);
        params->meta = NULL;
    }
}

int nc_dh_params_copy(struct nc_dh_params *params, struct nc_dh_params *src) {
    if(mpi_copy(&params->g, &src->p) != MPI_OK ||
       mpi_copy(&params->p, &src->p) != MPI_OK)
    {
        return NC_MALLOC;
    }

    return NC_OK;
}

int nc_dh_params_random(struct nc_dh_params *params, size_t prime_size) {
    mpi_from_uint(&params->g, 2);
    return mpi_random_prime(&params->p, 25, prime_size, MPI_PRIME_SAFE);
}

int nc_dh_params_from_bin(struct nc_dh_params *params, void *g, size_t g_size,
                                                       void *p, size_t p_size)
{
    int e;

    e = mpi_import(&params->g, g, g_size, 0, 1, 0);
    if(e != MPI_OK)
        return NC_MALLOC;

    e = mpi_import(&params->p, p, p_size, 0, 1, 0);
    if(e != MPI_OK)
        return NC_MALLOC;

    return NC_OK;
}

int nc_dh_params_to_bin(struct nc_dh_params *params, void **g_ret, size_t *g_size_ret,
                                                     void **p_ret, size_t *p_size_ret)
{
    int e;
    size_t g_size,
           p_size;
    void *g,*p;

    if(g_ret != NULL) {
        g_size = mpi_byte_size(&params->g);

        g = malloc(g_size);
        if(g == NULL) {
            e = NC_MALLOC;
            goto e0;
        }

        e = mpi_export(&params->g, g, g_size, 0, 1, 0);
        if(e != NC_OK)
            goto e0;

        *g_ret = g;
        *g_size_ret = g_size;
    }

    if(p_ret != NULL) {
        p_size = mpi_byte_size(&params->p);

        p = malloc(p_size);
        if(p == NULL) {
            e = NC_MALLOC;
            goto e1;
        }

        e = mpi_export(&params->p, p, p_size, 0, 1, 0);
        if(e != NC_OK)
            goto e1;

        *p_ret = p;
        *p_size_ret = p_size;
    }

    return NC_OK;

e1: free(g);

e0: return e;
}

int nc_dh_params_from_hex(struct nc_dh_params *params, char *g, char *p) {
    int e;

    if((e = mpi_from_str(&params->g, 16, g, NULL)) != MPI_OK ||
       (e = mpi_from_str(&params->p, 16, p, NULL)) != MPI_OK)
    {
        if(e == MPI_MALLOC)
            return NC_MALLOC;
        else
            return NC_BAD_INPUT;
    }

    return NC_OK;
}

int nc_dh_params_to_hex(struct nc_dh_params *params, char **g_ret, char **p_ret) {
    int e;
    char *g,*p;

    e = mpi_to_str(&params->g, 16, NULL, &g);
    if(e != NC_OK)
        goto e0;

    e = mpi_to_str(&params->p, 16, NULL, &p);
    if(e != NC_OK)
        goto e1;

    *g_ret = g;
    *p_ret = p;

    return NC_OK;

e1: free(g);

e0: return e;
}

int nc_dh_params_deserialize(struct nc_dh_params *params, char *body, size_t len) {
    nc_dh_params_clear(params);

    struct tag_deserialize_tpl tplv[] = {
        { "g",       TPL_MPI, &params->g,    NULL, true  },
        { "p",       TPL_MPI, &params->p,    NULL, true  },
        { "meta",    TPL_STR, &params->meta, NULL, false },
    //  { "version", TPL_STR, &version,      NULL, false }
    };

    return tag_deserialize(body, len, tplv, sizeof(tplv)/sizeof(tplv[0]));
}

int nc_dh_params_serialize(struct nc_dh_params *params, char **tag_ret, size_t *len_ret) {
    char version[64];

    snprintf(version, sizeof(version), "nocrypt %s", nc_version_str);

    struct tag_serialize_tpl tplv[] = {
        { "g",       TPL_MPI, &params->g,   -1 },
        { "p",       TPL_MPI, &params->p,   -1 },
        { "meta",    TPL_STR, params->meta, -1 },
        { "version", TPL_STR, version,      -1 }
    };

    return tag_serialize(NC_TAG_DH_PARAMETERS, tplv, sizeof(tplv)/sizeof(tplv[0]), tag_ret, len_ret);
}

int nc_dh_params_set_meta(struct nc_dh_params *params, char *meta) {
    meta = strdup(meta);
    if(meta == NULL)
        return NC_MALLOC;

    if(params->meta != NULL)
        free(params->meta);

    params->meta = meta;

    return NC_OK;
}

// DSA
// ----

int nc_dsa_key_init(struct nc_dsa_key *k) {
    int e;

    e = mpi_init(&k->v);
    if(e != MPI_OK)
        return NC_MALLOC;

    e = nc_dsa_params_init(&k->params);
    if(e != MPI_OK)
        return e;

    k->meta = NULL;

    return NC_OK;
}

void nc_dsa_key_clear(struct nc_dsa_key *k) {
    mpi_clear(&k->v);
    nc_dsa_params_clear(&k->params);
    if(k->meta != NULL) {
        free(k->meta);
        k->meta = NULL;
    }
}

int nc_dsa_key_copy(struct nc_dsa_key *k, struct nc_dsa_key *src) {
    int e;

    e = mpi_copy(&k->v, &src->v);
    if(e != MPI_OK)
        return NC_MALLOC;

    return nc_dsa_params_copy(&k->params, &src->params);
}

int nc_dsa_key_set_meta(struct nc_dsa_key *k, char *meta) {
    if(meta != NULL) {
        meta = strdup(meta);
        if(meta == NULL)
            return NC_MALLOC;
    }

    if(k->meta != NULL)
        free(k->meta);

    k->meta = meta;

    return NC_OK;
}

int nc_dsa_key_random(struct nc_dsa_key *k, ssize_t private_size, ssize_t prime_size) {
    int e;

    if(private_size >= 0) {
        e = mpi_random(&k->v, private_size, MPI_NONZERO);
        if(e != MPI_OK)
            return e;
    }
    
    if(prime_size >= 0) {
        e = nc_dsa_params_random(&k->params, prime_size);
        if(e != NC_OK)
            return e;
    }

    return NC_OK;
}

int nc_dsa_key_gen_public(struct nc_dsa_key *x, struct nc_dsa_key *X) {
    int e;

    e = mpi_powm(&X->v, &x->params.g, &x->v, &x->params.p);
    if(e == MPI_MALLOC)
        return NC_MALLOC;
    else if(e != MPI_OK)
        return NC_BAD_INPUT;

    if(mpi_copy(&X->params.g, &x->params.g) != MPI_OK ||
       mpi_copy(&X->params.p, &x->params.p) != MPI_OK ||
       mpi_copy(&X->params.q, &x->params.q) != MPI_OK)
    {
        return NC_MALLOC;
    }

    return NC_OK;
}

int nc_dsa_key_set_params(struct nc_dsa_key *k, struct nc_dsa_params *params) {
    if(mpi_copy(&k->params.g, &params->g) != MPI_OK ||
       mpi_copy(&k->params.p, &params->p) != MPI_OK ||
       mpi_copy(&k->params.q, &params->q) != MPI_OK)
    {
        return NC_MALLOC;
    }

    return NC_OK;
}

void nc_dsa_key_get_params(struct nc_dsa_key *k, struct nc_dsa_params **params_ret) {
    *params_ret = &k->params;
}

int nc_dsa_key_from_bin(struct nc_dsa_key *k, void *v, size_t v_size,
                                              void *g, size_t g_size,
                                              void *p, size_t p_size,
                                              void *q, size_t q_size)
{
    int e;

    e = mpi_import(&k->v, v, v_size, 0, 1, 0);
    if(e != MPI_OK)
        return NC_MALLOC;
    
    return nc_dsa_params_from_bin(&k->params, g, g_size, p, p_size, q, q_size);
}

int nc_dsa_key_to_bin(struct nc_dsa_key *k, void **v_ret, size_t *v_size_ret,
                                            void **g_ret, size_t *g_size_ret,
                                            void **p_ret, size_t *p_size_ret,
                                            void **q_ret, size_t *q_size_ret)
{
    int e;
    size_t v_size;
    void *v;

    if(v_ret != NULL) {
        v_size = mpi_byte_size(&k->v);

        v = malloc(v_size);
        if(v == NULL) {
            e = NC_MALLOC;
            goto e0;
        }

        e = mpi_export(&k->v, v, v_size, 0, 1, 0);
        if(e != NC_OK)
            goto e1;

        *v_ret = v;
    }

    e = nc_dsa_params_to_bin(&k->params, g_ret, g_size_ret, p_ret, p_size_ret, q_ret, q_size_ret);
    if(e != NC_OK)
        goto e1;

    return NC_OK;

e1: free(v);

e0: return e;
}

int nc_dsa_key_from_hex(struct nc_dsa_key *k, char *v, char *g, char *p, char *q) {
    int e;

    if((e = mpi_from_str(&k->v, 16, v, NULL)) != MPI_OK ||
       (e = mpi_from_str(&k->params.g, 16, g, NULL)) != MPI_OK ||
       (e = mpi_from_str(&k->params.p, 16, p, NULL)) != MPI_OK ||
       (e = mpi_from_str(&k->params.q, 16, q, NULL)) != MPI_OK)
    {
        if(e == MPI_MALLOC)
            return NC_MALLOC;
        else
            return NC_BAD_INPUT;
    }

    return NC_OK;
}

int nc_dsa_key_to_hex(struct nc_dsa_key *k, char **v_ret, char **g_ret, char **p_ret, char **q_ret) {
    int e;
    char *v;

    e = mpi_to_str(&k->v, 16, NULL, &v);
    if(e != NC_OK)
        goto e0;

    *v_ret = v;

    e = nc_dsa_params_to_hex(&k->params, g_ret, p_ret, q_ret);
    if(e != NC_OK)
        goto e1;

    return NC_OK;

e1: free(v);

e0: return e;
}

int nc_dsa_key_deserialize(struct nc_dsa_key *k, char *body, size_t len) {
    nc_dsa_key_clear(k);

    struct tag_deserialize_tpl tplv[] = {
        { NULL,      TPL_MPI, &k->v,        NULL, true  },
        { "g",       TPL_MPI, &k->params.g, NULL, true  },
        { "p",       TPL_MPI, &k->params.p, NULL, true  },
        { "q",       TPL_MPI, &k->params.q, NULL, true  },
        { "meta",    TPL_STR, &k->meta,     NULL, false },
    //  { "version", TPL_STR, &version,     NULL, false }
    };

    return tag_deserialize(body, len, tplv, sizeof(tplv)/sizeof(tplv[0]));
}

int nc_dsa_key_serialize(struct nc_dsa_key *k, char **tag_ret, size_t *len_ret) {
    char version[64];

    snprintf(version, sizeof(version), "nocrypt %s", nc_version_str);

    struct tag_serialize_tpl tplv[] = {
        { NULL,      TPL_MPI, &k->v,        -1 },
        { "g",       TPL_MPI, &k->params.g, -1 },
        { "p",       TPL_MPI, &k->params.p, -1 },
        { "q",       TPL_MPI, &k->params.q, -1 },
        { "meta",    TPL_STR, k->meta,      -1 },
        { "version", TPL_STR, version,      -1 }
    };

    return tag_serialize(NC_TAG_DSA_KEY, tplv, sizeof(tplv)/sizeof(tplv[0]), tag_ret, len_ret);
}

int nc_dsa_params_init(struct nc_dsa_params *params) {
    int e;

    e = mpi_init_multi(&params->p, &params->q, &params->g, NULL);
    if(e != MPI_OK)
        return NC_MALLOC;

    params->meta = NULL;

    return NC_OK;
}

void nc_dsa_params_clear(struct nc_dsa_params *params) {
    mpi_clear_multi(&params->g, &params->p, &params->q, NULL);
    if(params->meta != NULL) {
        free(params->meta);
        params->meta = NULL;
    }
}

int nc_dsa_params_copy(struct nc_dsa_params *params, struct nc_dsa_params *src) {
    if(mpi_copy(&params->g, &src->g) != MPI_OK ||
       mpi_copy(&params->p, &src->p) != MPI_OK ||
       mpi_copy(&params->q, &src->q) != MPI_OK)
    {
        return NC_MALLOC;
    }

    return NC_OK;
}

int nc_dsa_params_set_meta(struct nc_dsa_params *params, char *meta) {
    if(meta != NULL) {
        meta = strdup(meta);
        if(meta == NULL)
            return NC_MALLOC;
    }

    if(params->meta != NULL)
        free(params->meta);

    params->meta = meta;

    return NC_OK;
}

int nc_dsa_params_random(struct nc_dsa_params *params, size_t prime_size) {
    int e;
    mpi_t p,q,g;

    e = mpi_init_multi(&p,&q,&g, NULL);
    if(e != MPI_OK) {
        e = NC_MALLOC;
        goto r0;
    }

    mpi_random_prime(&p, 25, prime_size, MPI_PRIME_SAFE);

    e = mpi_rshift(&q,&p,1);
    if(e != MPI_OK) {
        e = NC_BAD_INPUT;
        goto r1;
    }

    if(mpi_copy(&params->g, &g) != MPI_OK ||
       mpi_copy(&params->p, &p) != MPI_OK ||
       mpi_copy(&params->q, &q) != MPI_OK)
    {
        e = NC_MALLOC;
        goto r1;
    }

r1: mpi_clear_multi(&p,&q,&g, NULL);

r0: return e;
}

int nc_dsa_params_from_bin(struct nc_dsa_params *params, void *g, size_t g_len,
                                                         void *p, size_t p_len,
                                                         void *q, size_t q_len)
{
    if(mpi_import(&params->g, g, g_len, 0, 1, 0) != MPI_OK ||
       mpi_import(&params->p, p, p_len, 0, 1, 0) != MPI_OK ||
       mpi_import(&params->q, q, q_len, 0, 1, 0) != MPI_OK)
    {
        return NC_MALLOC;
    }

    return NC_OK;
}

int nc_dsa_params_to_bin(struct nc_dsa_params *params, void **g_ret, size_t *g_len_ret,
                                                       void **p_ret, size_t *p_len_ret,
                                                       void **q_ret, size_t *q_len_ret)
{
    int e;
    void *g,*p,*q;
    size_t g_len,
           p_len,
           q_len;

    g_len = mpi_byte_size(&params->g);

    g = malloc(g_len);
    if(g == NULL) {
        e = NC_MALLOC;
        goto e0;
    }

    e = mpi_export(&params->g, g, g_len, 0, 1, 0);
    if(e != NC_OK)
        goto e1;

    p_len = mpi_byte_size(&params->p);

    p = malloc(p_len);
    if(p == NULL) {
        e = NC_MALLOC;
        goto e1;
    }

    e = mpi_export(&params->p, p, p_len, 0, 1, 0);
    if(e != NC_OK)
        goto e1;

    q_len = mpi_byte_size(&params->q);

    q = malloc(q_len);
    if(q == NULL) {
        e = NC_MALLOC;
        goto e2;
    }

    e = mpi_export(&params->q, q, q_len, 0, 1, 0);
    if(e != NC_OK)
        goto e2;

    *g_ret = g;
    *g_len_ret = g_len;
    *p_ret = p;
    *p_len_ret = p_len;
    *q_ret = q;
    *q_len_ret = q_len;

    return NC_OK;

e2: free(p);
e1: free(g);

e0: return e;
}

int nc_dsa_params_from_hex(struct nc_dsa_params *params, char *g, char *p, char *q) {
    if(mpi_from_str(&params->g, 16, g, NULL) != MPI_OK ||
       mpi_from_str(&params->p, 16, p, NULL) != MPI_OK ||
       mpi_from_str(&params->q, 16, q, NULL) != MPI_OK)
    {
        return NC_BAD_INPUT;
    }

    return NC_OK;
}

int nc_dsa_params_to_hex(struct nc_dsa_params *params, char **g_ret, char **p_ret, char **q_ret) {
    int e;
    char *g,*p,*q;

    e = mpi_to_str(&params->g, 16, NULL, &g);
    if(e != NC_OK)
        goto e0;

    e = mpi_to_str(&params->p, 16, NULL, &p);
    if(e != NC_OK)
        goto e1;

    e = mpi_to_str(&params->q, 16, NULL, &q);
    if(e != NC_OK)
        goto e2;

    *g_ret = g;
    *p_ret = p;
    *q_ret = q;

    return NC_OK;

e2: free(p);
e1: free(g);

e0: return e;
}

int nc_dsa_params_deserialize(struct nc_dsa_params *params, char *body, size_t len) {
    nc_dsa_params_clear(params);

    struct tag_deserialize_tpl tplv[] = {
        { "g",       TPL_MPI,  &params->g,     NULL, true },
        { "p",       TPL_MPI,  &params->p,     NULL, true },
        { "q",       TPL_MPI,  &params->q,     NULL, true },
        { "meta",    TPL_STR,  &params->meta,  NULL, false },
    //  { "version", TPL_STR,  &version,       NULL, false }
    };

    return tag_deserialize(body, len, tplv, sizeof(tplv)/sizeof(tplv[0]));
}

int nc_dsa_params_serialize(struct nc_dsa_params *params, char **tag_ret, size_t *len_ret) {
    char version[64];

    snprintf(version, sizeof(version), "nocrypt %s", nc_version_str);

    struct tag_serialize_tpl tplv[] = {
        { "g",       TPL_MPI, &params->g,   -1 },
        { "p",       TPL_MPI, &params->p,   -1 },
        { "q",       TPL_MPI, &params->q,   -1 },
        { "meta",    TPL_STR, params->meta, -1 },
        { "version", TPL_STR, version,      -1 }
    };

    return tag_serialize(NC_TAG_DSA_PARAMETERS, tplv, sizeof(tplv)/sizeof(tplv[0]), tag_ret, len_ret);
}

int nc_dsa_sign(uint8_t *hash, size_t hash_len, struct nc_dsa_key *x, struct nc_dsa_sig *sig) {
    int e;
    mpi_t h,a,b,k,r,ki,s;

    mpi_init_multi(&h,&k,&r,&ki,&s, NULL);
    mpi_import(&h, hash, hash_len, 0, 1, 0);

    while(true) {
        e = mpi_copy(&b, &x->params.q);
        if(e != MPI_OK)
            goto r1;

        // select k where 0 < k < q-1
        mpi_random_range(&k, &a, &b, MPI_NONZERO);

        // r = (g ^ k mod p) mod q
        mpi_powm(&r, &x->params.g, &k, &x->params.p);
        mpi_mod(&r, &r, &x->params.q);

        // if r == 0 reselect k
        if(mpi_cmp_uint(&r,0) == 0)
            continue;

        // ki = k ^ -1 mod q
        mpi_inv_mod(&ki, &k, &x->params.q);

        // s = (k ^ -1) * (h+(r*x))
        mpi_mul(&s,&r,&x->v);
        mpi_add(&s,&h,&s);
        mpi_mul(&s,&ki,&s);

        // if s == 0 reselect k
        if(mpi_cmp_uint(&s,0) == 0)
            continue;

        break;
    }

    mpi_copy(&sig->r, &r);
    mpi_copy(&sig->s, &s);

r1: mpi_clear_multi(&h,&k,&r,&ki,&s, NULL);

    return e;
}

int nc_dsa_verify(struct nc_dsa_sig *sig, uint8_t *hash, size_t hash_len, struct nc_dsa_key *Y) {
    int e;
    mpi_t h,w,u1,u2,e1,e2,v;

    mpi_init_multi(&h,&w,&u1,&u2,&e1,&e2,&v, NULL);
    mpi_import(&h, hash, hash_len, 0, +1, 0);

    // w = s ^ -1 mod q
    mpi_inv_mod(&sig->s, &Y->params.q, &w);

    // u1 = h * w mod q
    mpi_mul(&h, &w, &u1);
    mpi_mod(&u1, &Y->params.q, &u1);

    // u2 = r * w mod q
    mpi_mul(&sig->r, &w, &u2);
    mpi_mod(&u2, &Y->params.q, &u2);

    // v = ((g ^ u1) * (Y ^ u2) mod p) mod q
    mpi_powm(&Y->params.g, &u1, &Y->params.p, &e1);
    mpi_powm(&Y->v, &u2, &Y->params.p, &e2);
    mpi_mul(&e1, &e2, &v);
    mpi_mod(&v, &Y->params.p, &v);
    mpi_mod(&v, &Y->params.q, &v);

    if(mpi_cmp(&v, &sig->r) == 0)
        e = NC_OK;
    else
        e = NC_BAD_SIGNATURE;

r1: mpi_clear_multi(&h,&w,&u1,&u2,&e1,&e2,&v, NULL);

    return e;
}

int nc_dsa_sig_init(struct nc_dsa_sig *sig) {
    int e;

    e = mpi_init_multi(&sig->r, &sig->s, NULL);
    if(e != MPI_OK)
        return NC_MALLOC;

    sig->meta = NULL;

    return NC_OK;
}

int nc_dsa_sig_new(struct nc_dsa_sig **sig_ret) {
    int e;
    struct nc_dsa_sig *sig;

    sig = malloc(sizeof(struct nc_dsa_sig));
    if(sig == NULL) {
        e = NC_MALLOC;
        goto r0;
    }

    e = nc_dsa_sig_init(sig);
    if(e != NC_OK)
        goto r1;

    *sig_ret = sig;

    return NC_OK;

r1: free(sig);

r0: return e;
}

void nc_dsa_sig_clear(struct nc_dsa_sig *sig) {
    mpi_clear_multi(&sig->r, &sig->s, NULL);
    if(sig->meta != NULL) {
        free(sig->meta);
        sig->meta = NULL;
    }
}

void nc_dsa_sig_destroy(struct nc_dsa_sig *sig) {
    nc_dsa_sig_clear(sig);
    free(sig);
}

int nc_dsa_sig_copy(struct nc_dsa_sig *dest, struct nc_dsa_sig *src) {
    nc_dsa_sig_clear(dest);

    mpi_copy(&dest->r, &src->r);
    mpi_copy(&dest->s, &src->s);

    if(src->meta != NULL) {
        dest->meta = strdup(src->meta);
        if(dest->meta == NULL)
            return NC_MALLOC;
    }

    return NC_OK;
}

int nc_dsa_sig_set_meta(struct nc_dsa_sig *sig, char *meta) {
    char *tmp;

    if(meta != NULL) {
        tmp = strdup(meta);
        if(tmp == NULL)
            return NC_MALLOC;
    }
    else {
        tmp = NULL;
    }

    if(sig->meta != NULL)
        free(sig->meta);

    sig->meta = tmp;

    return NC_OK;
}

int nc_dsa_sig_from_bin(struct nc_dsa_sig **sig_ret, void *r, size_t r_len,
                                                     void *s, size_t s_len)
{
    int e;
    struct nc_dsa_sig *sig;

    sig = malloc(sizeof(struct nc_dsa_sig));
    if(sig == NULL) {
        e = NC_MALLOC;
        goto e0;
    }

    memset(sig, 0, sizeof(struct nc_dsa_sig));

    if(mpi_import(&sig->r, r, r_len, 0, 1, 0) == MPI_MALLOC ||
       mpi_import(&sig->s, s, s_len, 0, 1, 0) == MPI_MALLOC)
    {
        e = NC_MALLOC;
        goto e1;
    }

    *sig_ret = sig;

    return NC_OK;

e1: free(sig);

e0: return e;
}

int nc_dsa_sig_to_bin(struct nc_dsa_sig *sig, void **r_ret, size_t *r_len_ret,
                                              void **s_ret, size_t *s_len_ret)
{
    int e;
    size_t r_len,
           s_len;
    void *r,*s;

    r_len = mpi_byte_size(&sig->r);

    r = malloc(r_len);
    if(r != NULL) {
        e = NC_MALLOC;
        goto e0;
    }

    e = mpi_export(&sig->r, r, r_len, 0, 1, 0);
    if(e != NC_OK)
        goto e1;

    s_len = mpi_byte_size(&sig->s);

    s = malloc(s_len);
    if(s != NULL) {
        e = NC_MALLOC;
        goto e1;
    }

    e = mpi_export(&sig->s, s, s_len, 0, 1, 0);
    if(e != NC_OK)
        goto e2;

    *r_ret = r;
    *r_len_ret = r_len;
    *s_ret = s;
    *s_len_ret = s_len;

    return NC_OK;

e2: free(s);
e1: free(r);

e0: return e;
}

int nc_dsa_sig_from_hex(struct nc_dsa_sig **sig_ret, char *r_hex, char *s_hex) {
    int e;
    struct nc_dsa_sig *sig;

    sig = malloc(sizeof(struct nc_dsa_sig));
    if(sig == NULL) {
        e = NC_MALLOC;
        goto e0;
    }

    memset(sig, 0, sizeof(struct nc_dsa_sig));

    if((e = mpi_from_str(&sig->r, 16, r_hex, NULL)) != MPI_OK ||
       (e = mpi_from_str(&sig->s, 16, s_hex, NULL)) != MPI_OK)
    {
        goto e1;
    }

    *sig_ret = sig;

    return NC_OK;

e1: free(sig);

e0: return e;
}

int nc_dsa_sig_to_hex(struct nc_dsa_sig *sig, char **r_hex_ret, char **s_hex_ret) {
    int e;
    char *r,*s;

    e = mpi_to_str(&sig->r, 16, NULL, &r);
    if(e != NC_OK)
        goto e0;

    e = mpi_to_str(&sig->s, 16, NULL, &s);
    if(e != NC_OK)
        goto e1;

    *r_hex_ret = r;
    *s_hex_ret = s;

    return NC_OK;

e1: free(r);

e0: return e;
}

int nc_dsa_sig_deserialize(struct nc_dsa_sig *sig, char *body, size_t len) {
    nc_dsa_sig_clear(sig);

    struct tag_deserialize_tpl tplv[] = {
        { "r",       TPL_MPI, &sig->r,    NULL, true  },
        { "s",       TPL_MPI, &sig->s,    NULL, true  },
        { "meta",    TPL_STR, &sig->meta, NULL, false },
    //  { "version", TPL_STR, &version,   NULL, false }
    };

    return tag_deserialize(body, len, tplv, sizeof(tplv)/sizeof(tplv[0]));
}

int nc_dsa_sig_serialize(struct nc_dsa_sig *sig, char **tag_ret, size_t *len_ret) {
    char version[64];

    snprintf(version, sizeof(version), "nocrypt %s", nc_version_str);

    struct tag_serialize_tpl tplv[] = {
        { "r",       TPL_MPI, &sig->r,   -1 },
        { "s",       TPL_MPI, &sig->s,   -1 },
        { "meta",    TPL_STR, sig->meta, -1 },
        { "version", TPL_STR, version,   -1 }
    };

    return tag_serialize(NC_TAG_DSA_SIGNATURE, tplv, sizeof(tplv)/sizeof(tplv[0]), tag_ret, len_ret);
}

void nc_dsa_signed_data_init(struct nc_dsa_signed_data *sd) {
    memset(sd, 0, sizeof(struct nc_dsa_signed_data));
}

int nc_dsa_signed_data_new(struct nc_dsa_signed_data **sd_ret) {
    int e;
    struct nc_dsa_signed_data *sd;

    sd = malloc(sizeof(struct nc_dsa_signed_data));
    if(sd == NULL)
        return NC_MALLOC;

    nc_dsa_signed_data_init(sd);

    *sd_ret = sd;

    return NC_OK;
}

void nc_dsa_signed_data_clear(struct nc_dsa_signed_data *sd) {
    nc_data_clear(&sd->data);
    nc_dsa_sig_clear(&sd->sig);

    if(sd->meta != NULL) {
        free(sd->meta);
        sd->meta = NULL;
    }
}

void nc_dsa_signed_data_destroy(struct nc_dsa_signed_data *sd) {
    nc_dsa_signed_data_clear(sd);
    free(sd);
}

int nc_dsa_signed_data_assign(struct nc_dsa_signed_data *sd, uint8_t *data, size_t len, size_t alloc_len, struct nc_dsa_sig *sig) {
    int e;

    nc_data_assign(&sd->data, data, len, alloc_len);

    e = nc_dsa_sig_copy(&sd->sig, sig);
    if(e != NC_OK)
        return e;

    return NC_OK;
}

int nc_dsa_signed_data_release(struct nc_dsa_signed_data *sd, struct nc_data *data_out, struct nc_dsa_sig *sig_out) {
    int e;

    if(data_out != NULL) {
        nc_data_assign(data_out, sd->data.buf, sd->data.len, sd->data.alloc_len);
        data_out->meta = sd->meta;
        sd->meta = NULL;
    }

    memset(&sd->data, 0, sizeof(struct nc_data));

    if(sig_out != NULL) {
        e = nc_dsa_sig_copy(sig_out, &sd->sig);
        if(e != NC_OK)
            return e;
    }

    nc_dsa_sig_clear(&sd->sig);

    return NC_OK;
}

int nc_dsa_signed_data_set(struct nc_dsa_signed_data *sd, uint8_t *data, size_t len, struct nc_dsa_sig *sig) {
    int e;

    e = nc_data_set(&sd->data, data, len);
    if(e != NC_OK)
        return e;

    if(sig != NULL) {
        e = nc_dsa_sig_copy(&sd->sig, sig);
        if(e != NC_OK)
            return e;
    }

    return NC_OK;
}

int nc_dsa_signed_data_set_meta(struct nc_dsa_signed_data *sd, char *meta) {
    if(meta != NULL) {
        meta = strdup(meta);
        if(meta == NULL)
            return NC_MALLOC;
    }

    if(sd->meta != NULL)
        free(sd->meta);

    sd->meta = meta;

    return NC_OK;
}

void nc_dsa_signed_data_get(struct nc_dsa_signed_data *sd, struct nc_data **data_ret) {
    *data_ret = &sd->data;
}

int nc_dsa_signed_data_deserialize(struct nc_dsa_signed_data *sd, char *body, size_t len) {
    nc_dsa_signed_data_clear(sd);

    struct tag_deserialize_tpl tplv[] = {
        { NULL,      TPL_BIN, &sd->data.buf, &sd->data.len, true  },
        { "r",       TPL_MPI, &sd->sig.r,    NULL,          true  },
        { "s",       TPL_MPI, &sd->sig.s,    NULL,          true  },
        { "meta",    TPL_STR, &sd->meta,     NULL,          false },
    //  { "version", TPL_STR, &version,      NULL,          false }
    };

    return tag_deserialize(body, len, tplv, sizeof(tplv)/sizeof(tplv[0]));
}

int nc_dsa_signed_data_serialize(struct nc_dsa_signed_data *sd, char **tag_ret, size_t *len_ret) {
    char version[64];

    snprintf(version, sizeof(version), "nocrypt %s", nc_version_str);

    struct tag_serialize_tpl tplv[] = {
        { NULL,      TPL_BIN, sd->data.buf, sd->data.len },
        { "r",       TPL_MPI, &sd->sig.r,   -1           },
        { "s",       TPL_MPI, &sd->sig.s,   -1           },
        { "meta",    TPL_STR, sd->meta,     -1           },
        { "version", TPL_STR, version,      -1,          }
    };

    return tag_serialize(NC_TAG_DSA_SIGNED_DATA, tplv, sizeof(tplv)/sizeof(tplv[0]), tag_ret, len_ret);
}

void nc_dsa_signed_data_hash(struct nc_dsa_signed_data *sd, struct nc_key *hk, uint8_t *hash_out) {
    nc_hash_auto(hk, sd->data.buf, sd->data.len, hash_out);
}

int nc_dsa_signed_data_verify(struct nc_dsa_signed_data *sd, struct nc_key *hk, struct nc_dsa_key *Y) {
    int e;
    uint8_t *hash;

    hash = malloc(hk->size);
    if(hash == NULL)
        return NC_MALLOC;

    nc_dsa_signed_data_hash(sd, hk, hash);
    e = nc_dsa_verify(&sd->sig, hash, hk->size, Y);

    free(hash);
    return e;
}
